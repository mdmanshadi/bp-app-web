<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/logout','Auth\LoginController@logout')->name('logout');

//دانلود فایل درس ها
Route::get('/dlFile/{token}/start','DownloadController@dlFile')->name('dlFile');
//پرداخت یک تراکنش و ارسال به بانک
Route::get('/payment/{id}/start','DownloadController@PayInvoice')->name('payment');
//نمایش عملیات موفق پرداخت
Route::get('/success/','DownloadController@success')->name('success');

//آدرس بازگشت از بانک
Route::get('/callBack/{id}/payment','DownloadController@callBack')->name('callBack');

//نمایش وب ویو صفحات ثابت
Route::get('/showStatic/{id}','DownloadController@showStatic')->name('showStatic');

//برای نمایش این روت ها حتما کاربر باید لاگین باشد (مدیریت)
Route::middleware(['auth'])->group(function () {

    //داشبورد
    Route::get('/', 'HomeController@dashboard')->name('home');

    //کاربر
    Route::any('/user', 'UserController@index')->name('admin.user.index')->middleware('can:read-users');
    Route::get('/user/create', 'UserController@create')->name('admin.user.create')->middleware('can:create-users');
    Route::get('/user/{id}/edit', 'UserController@edit')->name('admin.user.edit')->middleware('can:update-users');
    Route::post('/user/save', 'UserController@save')->name('admin.user.save');
    Route::get('/user/delete', 'UserController@delete')->name('admin.user.delete')->middleware('can:delete-users');
    Route::get('/user/{id}/lessons', 'UserController@lessons')->name('admin.user.lessons');
    Route::get('/user/{id}/transactions', 'UserController@transactions')->name('admin.user.transactions');
    Route::get('/user/{id}/exams', 'UserController@exams')->name('admin.user.exams');
    Route::get('/user/{id}/buy-manually', 'UserController@manual')->name('admin.user.manual');
    Route::post('/user/{id}/buy-manually', 'UserController@manualSave')->name('admin.user.manual.save');
    Route::get('/user/export-excel','UserController@exportExcel')->name('admin.user.export');
    Route::get('/user/import-excel-index','UserController@importIndex')->name('admin.user.import')->middleware('can:create-users');
    Route::post('/user/import-excel','UserController@importExcel')->name('admin.user.import.save');

     //پرسنل

    Route::get('/admins', 'AdminController@index')->name('admin.admin.index')->middleware('can:read-admins');
    Route::get('/admin/create', 'AdminController@create')->name('admin.admin.create')->middleware('can:create-admins');
    Route::get('/admin/{id}/edit', 'AdminController@edit')->name('admin.admin.edit')->middleware('can:update-admins');
    Route::post('/admin/save', 'AdminController@save')->name('admin.admin.save');
    Route::get('/admin/delete', 'AdminController@delete')->name('admin.admin.delete')->middleware('can:delete-admins');

//نقش ها و دسترسی ها

    Route::get('/roles', 'RoleController@index')->name('admin.roles.index')->middleware('can:read-roles');
    Route::get('/role/create', 'RoleController@create')->name('admin.role.create')->middleware('can:create-roles');
    Route::post('/role/save', 'RoleController@save')->name('admin.role.save');
    Route::get('/role/{id}/edit', 'RoleController@edit')->name('admin.role.edit')->middleware('can:update-roles');
    Route::get('/role/delete', 'RoleController@delete')->name('admin.role.delete')->middleware('can:delete-roles');

    //تراکنش ها
    Route::any('/transaction', 'TransactionController@index')->name('admin.transaction.index')->middleware('can:read-transactions');
    Route::any('/Notification', 'NotificationController@index')->name('admin.notification.index')->middleware('can:read-notifications');
    Route::any('/Notification/create', 'NotificationController@create')->name('admin.notification.create')->middleware('can:create-notifications');
    Route::any('/Notification/save', 'NotificationController@save')->name('admin.notification.save');
    Route::get('/Notification/delete', 'NotificationController@delete')->name('admin.notification.delete')->middleware('can:delete-notifications');


    //خرید های کاربران
    Route::any('/UserLesson', 'UserLessonController@index')->name('admin.userlesson.index')->middleware('can:read-checkouts');
    Route::get('/UserLesson/delete', 'UserLessonController@delete')->name('admin.userlesson.delete')->middleware('can:delete-checkouts');

    //آزمون های کاربران
    Route::any('/Quiz', 'QuizController@index')->name('admin.quiz.index')->middleware('can:read-quizzes');
    Route::get('/Quiz/{id}/detail', 'QuizController@detail')->name('admin.quiz.detail')->middleware('can:read-quizzes');
    Route::get('/Quiz/delete', 'QuizController@delete')->name('admin.quiz.delete')->middleware('can:delete-quizzes');
    Route::any('/QuizQuestion', 'QuizQuestionController@index')->name('admin.quizquestion.index')->middleware('can:read-quizzes');

    //دوره ها
    Route::any('/category', 'CategoryController@index')->name('admin.category.index')->middleware('can:read-contents');
    Route::get('/category/create', 'CategoryController@create')->name('admin.category.create')->middleware('can:create-contents');
    Route::get('/category/{id}/edit', 'CategoryController@edit')->name('admin.category.edit')->middleware('can:update-contents');
    Route::post('/category/save', 'CategoryController@save')->name('admin.category.save');
    Route::get('/category/delete', 'CategoryController@delete')->name('admin.category.delete')->middleware('can:delete-contents');

    //درس ها
    Route::any('/lesson', 'LessonController@index')->name('admin.lesson.index')->middleware('can:read-contents');
    Route::get('/lesson/create', 'LessonController@create')->name('admin.lesson.create')->middleware('can:create-contents');
    Route::get('/lesson/{id}/edit', 'LessonController@edit')->name('admin.lesson.edit')->middleware('can:update-contents');
    Route::any('/lesson/{id}/file', 'LessonController@file')->name('admin.lesson.file');

    Route::get('/lesson/{id}/question', 'LessonController@question')->name('admin.lesson.question');

    Route::get('/lesson/{id}/newfile', 'LessonController@newfile')->name('admin.lesson.newfile');
    Route::post('/lesson/{id}/savefile', 'LessonController@savefile')->name('admin.lesson.savefile');
    Route::get('/lesson/{id}/editfile', 'LessonController@editfile')->name('admin.lesson.editfile');
    Route::post('/lesson/save', 'LessonController@save')->name('admin.lesson.save');
    Route::get('/lesson/delete', 'LessonController@delete')->name('admin.lesson.delete')->middleware('can:delete-contents');
    Route::get('/lesson/file/delete', 'LessonController@fileDelete')->name('admin.lesson.file.delete');

    //سوال ها
    Route::any('/question', 'QuestionController@index')->name('admin.question.index');
    Route::post('/question/save', 'QuestionController@save')->name('admin.question.save');
    Route::get('/question/{id}/create', 'QuestionController@createQ')->name('admin.question.create');
    Route::get('/question/{id}/edit', 'QuestionController@edit')->name('admin.question.edit');
    Route::get('/question/delete', 'QuestionController@delete')->name('admin.question.delete');
    Route::any('/question/{id}/answer', 'QuestionController@answer')->name('admin.question.answer');

    //جواب ها
    Route::any('/answer', 'AnswerController@index')->name('admin.answer.index');
    Route::get('/answer/create', 'AnswerController@create')->name('admin.answer.create');
    Route::post('/answer/save', 'AnswerController@save')->name('admin.answer.save');
    Route::get('/answer/{id}/edit', 'AnswerController@edit')->name('admin.answer.edit');
    Route::get('/answer/delete', 'AnswerController@delete')->name('admin.answer.delete');


    Route::any('/exam/{id}/{type}', 'ExamController@exam')->name('admin.exam.create');




    //کد های تخفیف
    Route::any('/coupon', 'CouponController@index')->name('admin.coupon.index')->middleware('can:read-coupons');
    Route::get('/coupon/create', 'CouponController@create')->name('admin.coupon.create')->middleware('can:create-coupons');
    Route::get('/coupon/{id}/edit', 'CouponController@edit')->name('admin.coupon.edit')->middleware('can:update-coupons');
    Route::post('/coupon/save', 'CouponController@save')->name('admin.coupon.save');
    Route::get('/coupon/delete', 'CouponController@delete')->name('admin.coupon.delete')->middleware('can:delete-coupons');
    Route::get('/coupon/createBatch', 'CouponController@createBatch')->name('admin.coupon.createBatch');
    Route::post('/coupon/saveBatch', 'CouponController@saveBatch')->name('admin.coupon.saveBatch');


    //صفحات ثابت
    Route::any('/static', 'StaticController@index')->name('admin.static.index')->middleware('can:read-statics');
    Route::get('/static/create', 'StaticController@create')->name('admin.static.create')->middleware('can:create-coupons');
    Route::get('/static/{id}/edit', 'StaticController@edit')->name('admin.static.edit')->middleware('can:edit-coupons');
    Route::post('/static/save', 'StaticController@save')->name('admin.static.save');
    Route::get('/static/delete', 'StaticController@delete')->name('admin.static.delete')->middleware('can:delete-coupons');

    //اسلایدر ها
    Route::any('/slider', 'SliderController@index')->name('admin.slider.index')->middleware('can:read-sliders');
    Route::get('/slider/create', 'SliderController@create')->name('admin.slider.create')->middleware('can:create-coupons');
    Route::get('/slider/{id}/edit', 'SliderController@edit')->name('admin.slider.edit')->middleware('can:update-coupons');
    Route::post('/slider/save', 'SliderController@save')->name('admin.slider.save');
    Route::get('/slider/delete', 'SliderController@delete')->name('admin.slider.delete')->middleware('can:delete-coupons');


    //پکیج ها
    Route::any('/package', 'PackageController@index')->name('admin.package.index')->middleware('can:read-contents');
    Route::get('/package/create', 'PackageController@create')->name('admin.package.create')->middleware('can:create-contents');
    Route::get('/package/{id}/edit', 'PackageController@edit')->name('admin.package.edit')->middleware('can:update-contents');
    Route::post('/package/save', 'PackageController@save')->name('admin.package.save');
    Route::get('/package/delete', 'PackageController@delete')->name('admin.package.delete')->middleware('can:delete-contents');




});

