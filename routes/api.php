<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::any('{version}/{category}/{function}', 'ServiceController@parser')->name('service.parser');

Route::post('verify-token', 'NewApiController@verifyToken');
Route::post('login', 'NewApiController@login');
Route::get('main', 'NewApiController@main');
Route::get('get-course-details/{id}', 'NewApiController@getCourseDetails');
Route::get('get-lesson-details/{id}', 'NewApiController@getLessonDetails');
Route::get('get-package-details/{id}', 'NewApiController@getPackageDetails');
Route::get('get-file-details/{id}', 'NewApiController@getFileDetails')->middleware('token');
