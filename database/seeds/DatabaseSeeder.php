<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'admins',
            'users',
            'roles',
            'contents',
            'transactions',
            'checkouts',
            'coupons',
            'statics',
            'sliders',
            'quizzes',
            'notifications'
        ];
        foreach ($permissions as $permission){
            DB::table('permissions')->insert([
                'name' => "read-$permission",
                'guard_name' => 'web'
            ]);
            DB::table('permissions')->insert([
                'name' => "create-$permission",
                'guard_name' => 'web'
            ]);
            DB::table('permissions')->insert([
                'name' => "update-$permission",
                'guard_name' => 'web'
            ]);
            DB::table('permissions')->insert([
                'name' => "delete-$permission",
                'guard_name' => 'web'
            ]);
        }

        DB::table('roles')->insert([
            'name' => "admin",
            'guard_name' => 'web'
        ]);

        $permissions_id = range(1,44);
        foreach ($permissions_id as $id)
            DB::table('role_has_permissions')->insert([
                'permission_id' => $id,
                'role_id' => 1
            ]);
    }
}
