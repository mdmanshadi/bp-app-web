@extends('admin.layout')
@section('css')
    <link href="{{URL::to('/')}}/admin/assets/css/multi-select.css" media="screen" rel="stylesheet" type="text/css">
@endsection
@section('content')
    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">مدیریت دسترسی محتوا برای
                    ( {{$user->name}} )
                </h4>
            </div>
            <div class="panel-body">
                <form class="form-horizontal form" method="post" href="{{route('admin.user.manual.save',$user->id)}}">
                    @csrf
                    <h4>خرید پکیج</h4>
                    <hr>
                    <select id='packages-multiselect' multiple='multiple' name="packages[]">
                    @foreach($packages as $package)
                            <option value='{{$package->id}}' {{array_key_exists($package->id,$packagesData) ? 'selected' : ''}}>{{$package->title}}</option>
                    @endforeach
                    </select>
                    <br>
                    <h4>خرید دوره</h4>
                    <hr>
                    <select id='courses-multiselect' multiple='multiple' name="courses[]">
                        @foreach($courses as $course)
                            <option value='{{$course->id}}' {{array_key_exists($course->id,$coursesData) ? 'selected' : ''}}>{{$course->title}}</option>
                        @endforeach
                    </select>
                    <br>

                    <h4>خرید درس</h4>
                    <hr>
                    <select id='lessons-multiselect' multiple='multiple' name="lessons[]">
                        @foreach($lessons as $lesson)
                            <option value='{{$lesson->id}}' {{array_key_exists($lesson->id,$lessonsData) ? 'selected' : ''}}>{{$lesson->title}}</option>
                        @endforeach
                    </select>
                    <br>
                    <input type="submit" class="btn btn-primary" value="ثبت">
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{URL::to('/')}}/admin/assets/js/jquery.multi-select.js" type="text/javascript"></script>
    <script>
        $('#packages-multiselect').multiSelect({
            selectableHeader: 'پکیج های غیرفعال',
            selectionHeader: 'پکیج های فعال'
        });

        $('#courses-multiselect').multiSelect({
            selectableHeader: 'دوره های غیرفعال',
            selectionHeader: 'دوره های فعال'
        });

        $('#lessons-multiselect').multiSelect({
            selectableHeader: 'درس های های غیرفعال',
            selectionHeader: 'درس های فعال'
        });
    </script>
@endsection
