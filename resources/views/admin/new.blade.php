@extends('admin.layout')
@section('content')
    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">{{$title}}</h4>
            </div>
            <div class="panel-body">
                <form class="form-horizontal" method="post" action="
    @if(is_array($saveRoute))
        {{route($saveRoute[0] ?? 'home',$saveRoute[1])}}
    @else
        {{route($saveRoute ?? 'home')}}
    @endif
                " enctype="multipart/form-data">

                    @csrf
                    @if(!empty($id))
                        <input type="hidden" name="id" value="{{$id}}">
                    @endif
                    <?php $name=[]; ?>
                    @if(!empty($reqData))
                        @foreach($reqData as $rq=>$v)
                            <input type="hidden" name="{{$rq}}" value="{{$v}}">
                        @endforeach
                    @endif
                    @foreach($fields as $field)
                        <?php
                            if(!is_array($field))
                                $field = ["type"=>"text","name"=>$field];
                            if(empty($field['type']))
                                $field['type'] = 'text';
                            $name = $field['name'];
                        ?>
                        @if($field['type'] == 'text')
                            <div class="form-group">
                                <label for="input-Default"  class="col-sm-2 control-label">{{$field['caption'] ?? $field['name']}}</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="{{$field['name']}}" value="{{$data->$name ?? old($name)}}">
                                </div>
                            </div>
                        @elseif($field['type'] == 'password')
                            <div class="form-group">
                                <label for="input-Default" class="col-sm-2 control-label">{{$field['caption'] ?? $field['name']}}</label>
                                <div class="col-sm-10">
                                    <input type="password" class="form-control" placeholder="در صورت نیاز به تغییر مقدار جدید را وارد کنید" name="{{$field['name']}}" value="">
                                </div>
                            </div>
                        @elseif($field['type'] == 'disable')
                            <div class="form-group">
                                <label for="input-Default" class="col-sm-2 control-label">{{$field['caption'] ?? $field['name']}}</label>
                                <div class="col-sm-10">
                                    <input type="text" disabled class="form-control" name="{{$field['name']}}" value="{{$data->$name ?? old($name)}}">
                                </div>
                            </div>
                        @elseif($field['type'] == 'number')
                            <div class="form-group">
                                <label for="input-Default" class="col-sm-2 control-label">{{$field['caption'] ?? $field['name']}}</label>
                                <div class="col-sm-10">
                                    <input type="number" class="form-control" name="{{$field['name']}}" value="{{$data->$name ?? old($name)}}">
                                </div>
                            </div>
                        @elseif($field['type'] == 'date')
                                <div class="form-group">
                                    <label for="input-Default" class="col-sm-2 control-label">{{$field['caption'] ?? $field['name']}}</label>
                                    <div class="col-sm-10">
                                        <input type="text"  class="form-control datePicker" autocomplete="off" name="{{$field['name']}}" value="{{$data->$name ?? old($name)}}">
                                    </div>
                                </div>
                        @elseif($field['type'] == 'time')
                                <div class="form-group">
                                    <label for="input-Default" class="col-sm-2 control-label">{{$field['caption'] ?? $field['name']}}</label>
                                    <div class="col-sm-10">
                                        <input type="time" class="form-control" name="{{$field['name']}}" value="{{$data->$name ?? old($name)}}">
                                    </div>
                                </div>
                        @elseif($field['type'] == 'image')
                            <div class="form-group">
                                <label for="input-Default" class="col-sm-2 control-label">{{$field['caption'] ?? $field['name']}}</label>

                                <div class="col-sm-10">
                                    <input type="file" class="form-control" name="{{$field['name']}}">
                                </div>
                                @if(!empty($data->$name))
                                    <div class="col-sm-2">
                                    </div>
                                    <div class="col-sm-10">
                                        <img src="{{Storage::url($data->$name)}}" style="float: left;margin-left: 35%;padding: 20px" width="30%" >
                                    </div>
                                @endif
                            </div>
                        @elseif($field['type'] == 'file')
                            <div class="form-group">
                                <label for="input-Default" class="col-sm-2 control-label">{{$field['caption'] ?? $field['name']}}</label>

                                <div class="col-sm-10">
                                    <input type="file" class="form-control" name="{{$field['name']}}">
                                </div>
                            </div>
                        @elseif($field['type'] == 'select')
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">{{$field['caption'] ?? $field['name']}}</label>
                                    <div class="col-sm-10">
                                        <select class="form-control form-select-options select2" name="{{$field['name']}}">
                                            <option value="0">انتخاب کنید</option>
                                            @if(!empty($field['values']))
                                                @foreach($field['values'] as $id=>$val)
                                                    <option value="{{$id}}" @if(!empty($data->$name) && $data->$name == $id) selected @elseif(!empty($request) && $request->input($name) == $id) selected @endif>{{$val}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                        @elseif($field['type'] == 'multiselect')
                            <div class="form-group">
                                <label class="col-sm-2 control-label">{{$field['caption'] ?? $field['name']}}</label>
                                <div class="col-sm-10">
                                    <select multiple class="form-control form-select-options select2" name="{{$field['name']}}[]">
                                        <?php
                                        if(!empty($data->$name)){
                                            $f = json_decode($data->$name) ?? [];
                                            if(!is_array($f))
                                                $f = [];
                                        }
                                        ?>
                                        @if(!empty($field['values']))
                                            @foreach($field['values'] as $id=>$val)
                                                <option value="{{$id}}" @if(!empty($data->$name) && in_array($id,$f)) selected @elseif(!empty($request) && is_array($request->input($name)) && in_array($id,$request->input($name))) selected @endif>{{$val}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        @elseif($field['type'] == 'checkbox')
                            <div class="form-group">
                                <label class="col-sm-2 control-label"></label>
                                <div class="col-sm-10">
                                    <div class="checkbox">
                                        <label>
                                            <?php $nn = $field['dataName']; ?>
                                            <div class=""><span class=""><input @if(!empty($data->$nn[($field['id'])]) && $data->$nn[($field['id'])] == '1') checked @endif type="checkbox" name="{{$field['name'] ?? ''}}[{{$field['id']}}]" value="1"></span>{{$field['caption'] ?? $field['name']}}</div>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        @elseif($field['type'] == 'hidden')
                        @elseif($field['type'] == 'textarea')
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">{{$field['caption'] ?? $field['name']}}</label>
                                    <div class="col-sm-10">
                                        <textarea name="{{$field['name']}}" class="summernote">{{$data->$name ?? ''}}</textarea>
                                    </div>
                                </div>

                        @elseif($field['type'] == 'location')
                            <?php $hasmap = true;$names[] = empty($field['name']) ? 'n' : $field['name']; ?>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">{{$field['caption'] ?? $field['name']}}</label>
                                <div class="col-sm-10">
                                    برای انتخاب موقعیت مکانی چند ثانیه روی موقعیت مورد نظر کلیک کنید
                                    <div id="location_{{(empty($field['name']) ? 'n' : $field['name'])}}" style="height: 250px;"></div>
                                    <?php $lt = empty($field['name']) ? 'lat' : $field['name']."_lat"; ?>
                                    <?php $lg = empty($field['name']) ? 'lng' : $field['name']."_lng"; ?>
                                    <input type="hidden" name="{{$lt}}" value="{{$data->$lt ?? ''}}">
                                    <input type="hidden" name="{{$lg}}" value="{{$data->$lg ?? ''}}">
                                </div>

                            </div>
                        @elseif($field['type'] == 'tag')
                            <div class="form-group">
                                <label class="col-sm-2 control-label">{{$field['caption'] ?? $field['name']}}</label>
                                <div class="col-sm-10">
                                    <input type="text" name="{{$field['name']}}" value="{{$data->$name ?? ''}}" data-role="tagsinput" class="form-control">
                                </div>
                            </div>
                        @else

                            not supported
                        @endif

                    @endforeach

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-success horizontal-form-button">ذخیره اطلاعات</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $('.summernote').summernote({
            height: 350,
            fontNames: ['IRANYekan', 'tahoma'],
        });
    </script>
    @if(!empty($hasmap))
    <script async defer src="https://maps.googleapis.com/maps/api/js?callback=initMap&language=fa&region=IR"></script>

    <script>
        $(window).on("load",function (e) {
            $("span:contains('Google Maps')").parent().parent().hide();
        })



        var timer, marker;
        function initMap() {
            @foreach($names as $n)
                var map_{{$n}} = new google.maps.Map(document.getElementById('location_{{$n}}'), {
                    zoom: 14,
                    center: {lat: {{$data->lat ?? 31.8922477}}, lng: {{$data->lng ?? 54.3605122}} },
                    mapTypeControl: false,
                    scaleControl: false,
                    streetViewControl: false,
                    fullscreenControl: false,
                    mapTypeId: "ParsijooMap",

                });
            map_{{$n}}.mapTypes.set("ParsijooMap", new google.maps.ImageMapType({
                    getTileUrl: function(coord, zoom) {
                        var tilesPerGlobe = 1 << zoom;
                        var x = coord.x % tilesPerGlobe;
                        if (x < 0) {
                            x = tilesPerGlobe+x;
                        }
                        return "https://developers.parsijoo.ir/web-service/v1/map/?type=tile"+"&" +"x=" + x + "&" +"y=" + coord.y + "&" +"z=" + zoom +"&" + "apikey=b79aa829a62541e1897c1fcc2807a177";
                    },
                    tileSize: new google.maps.Size(256, 256),
                    name: "ParsijooMap",
                    maxZoom: 18
                }));
            <?php $lt = ($n == 'n') ? 'lat' : $n."_lat"; ?>
            <?php $lg = ($n == 'n') ? 'lng' : $n."_lng"; ?>
            marker_{{$n}} = undefined;

                @if(!empty($data->$lt))
                if(marker_{{$n}} === undefined){
                    marker_{{$n}} = new google.maps.Marker({
                        position: {lat: {{$data->$lt ?? 31.8922477}}, lng: {{$data->$lg ?? 54.3605122}} },
                        map: map_{{$n}}
                    });
                }else{
                    marker_{{$n}}.setPosition({lat: {{$data->$lt ?? 31.8922477}}, lng: {{$data->$lg ?? 54.3605122}} });
                }

                @endif


                map_{{$n}}.addListener('mousedown', function(event){
                    timer = setTimeout(function(){
                        center = event.latLng;
                        if(marker_{{$n}} === undefined){
                            marker_{{$n}} = new google.maps.Marker({
                                position: center,
                                map: map_{{$n}}
                            });
                        }else{
                            marker_{{$n}}.setPosition({lat: center.lat(), lng: center.lng()});
                        }
                        $('input[name={{$lt}}]').val(center.lat());
                        $('input[name={{$lg}}]').val(center.lng());


                        // console.log(google.maps.geometry.poly.containsLocation(center, freeDelivery))

                        clearTimeout(timer);
                    }, 500);
                });
            map_{{$n}}.addListener('mouseup', function(event){
                    clearTimeout(timer);
                });
            map_{{$n}}.addListener('drag', function(event){
                    clearTimeout(timer);
                });
            @endforeach



        }
    </script>
    @endif

@endsection
