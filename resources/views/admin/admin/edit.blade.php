@extends('admin.layout')
@section('content')
    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">افزودن نقش جدید</h4>
            </div>
            <div class="panel-body">
                <form method="post" action="{{route('admin.admin.save',$admin->id)}}">
                    @csrf
                    <input type="hidden" name="id" value="{{ isset($id) ? $id : '' }}">
                    <div class="form-group">
                        <label for="name">نام</label>
                        <input id="name" type="text" name="name" class="form-control" value="{{$admin->name}}" required>
                    </div>

                    <div class="form-group">
                        <label for="username">نام کاربری</label>
                        <input id="username" type="text" name="username" class="form-control"
                               value="{{$admin->username}}" required>
                    </div>

                    <div class="form-group">
                        <label for="password">رمز عبور</label>
                        <input id="password" type="password" name="password" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="role">نقش</label>
                        <select name="role" id="role" class="form-control">
                            @foreach($roles as $role)
                                <option value="{{$role->name}}" {{$adminRole->id ===  $role->id ? 'selected' : ''}}>{{$role->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <button type="submit" class="btn btn-success horizontal-form-button">ذخیره</button>
                </form>
            </div>
        </div>
    </div>
@endsection
