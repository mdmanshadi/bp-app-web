
<!doctype html>
<html lang="fa">
<style>
    html{
        direction: rtl;
    }
    @font-face {
        font-family: IRANYekan;
        font-style: normal;
        src: url('{{asset('admin/assets/')}}/fonts/IRANYekan.woff2') format('woff2'), url('{{asset('admin/assets/')}}/fonts/IRANYekan.woff') format('woff'), url('{{asset('admin/assets/')}}/fonts/IRANYekan.ttf') format('ttf');
    }
    body{
        font-family: IRANYekan,tahoma !important;
        font-size: 14px;
        line-height: 2;
        padding: 15px;
    }
    td{
        border: 1px solid #ccc;
        padding: 5px;
        text-align: center;
    }

</style>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body style="display: flex;justify-content: center;align-items: center">
<table style="width: 297mm;padding: 2cm">
    <thead>
    <tr>
        @foreach($colsNames as $header)
            <th>{{$header}}</th>
        @endforeach
    </tr>
    </thead>
    <tbody>
    @foreach($rows as $row)
        <tr>
            @foreach($cols as $col)
                @if(strpos($col,"ToNFT") !== false)
                    <?php
                    $col = explode("ToNFT",$col)[0] ?? 'n';
                    ?>
                    <td>{!! (number_format($row->$col ?? '0')." تومان")  !!}</td>
                @elseif(strpos($col,"ToMetr") !== false)
                    <?php
                    $col = explode("ToMetr",$col)[0] ?? 'n';
                    ?>
                    <td>{!! (number_format($row->$col ?? '0')." متر")  !!}</td>
                @elseif(strpos($col,"ToPC") !== false)
                    <?php
                    $col = explode("ToPC",$col)[0] ?? 'n';
                    ?>
                    <td>{!! (number_format($row->$col ?? '0')." درصد")  !!}</td>
                @else
                    <td>{!! $row->$col !!}</td>
                @endif
            @endforeach
        </tr>
    @endforeach
    </tbody>
</table>
</body>
<script>
    window.print();
</script>
</html>