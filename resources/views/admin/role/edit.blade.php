@extends('admin.layout')
@section('content')
    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">افزودن نقش جدید</h4>
            </div>
            <div class="panel-body">
                <form method="post" action="{{route('admin.role.save',$role->id)}}">
                    @csrf
                    <div class="form-group">
                        <label for="name">نام نقش</label>
                        <input id="name" type="text" name="name" value="{{$role->name}}" class="form-control" required>
                        <hr>
                        <h5>مدیریت دسترسی ها</h5>
                        <br>
                        @foreach($permissions as $permission => $name)
                            <span>{{$name}}</span>
                            <hr>
                            <div class="form-group">
                                <input id="{{'read-'.$permission}}" type="checkbox" value="{{'read-'.$permission}}"
                                       {{in_array('read-'.$permission,$userPermissions) ? 'checked' : ''}}
                                       name="permissions[]">
                                <label for="{{'read-'.$permission}}">مشاهده</label>
                            </div>
                            <div class="form-group">
                                <input id="{{'create-'.$permission}}" type="checkbox" value="{{'create-'.$permission}}"
                                       {{in_array('create-'.$permission,$userPermissions) ? 'checked' : ''}}
                                       name="permissions[]">
                                <label for="{{'create-'.$permission}}">افزودن</label>
                            </div>
                            <div class="form-group">
                                <input id="{{'update-'.$permission}}" type="checkbox" value="{{'update-'.$permission}}"
                                       {{in_array('update-'.$permission,$userPermissions) ? 'checked' : ''}}
                                       name="permissions[]">
                                <label for="{{'update-'.$permission}}">ویرایش</label>
                            </div>
                            <div class="form-group">
                                <input id="{{'delete-'.$permission}}" type="checkbox" value="{{'delete-'.$permission}}"
                                       {{in_array('delete-'.$permission,$userPermissions) ? 'checked' : ''}}
                                       name="permissions[]">
                                <label for="{{'delete-'.$permission}}">حذف</label>
                            </div>
                            <br>
                        @endforeach
                        <button type="submit" class="btn btn-success horizontal-form-button">ذخیره</button>
                </form>
            </div>
        </div>
    </div>
@endsection
