@extends('admin.layout')
@section('content')
    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">دورن ریزی کاربران</h4>
            </div>
            <div class="panel-body">
                <a href="{{route('admin.user.export')}}"><p>برای دریافت فایل اکسل کاربران به عنوان نمونه کلیک کنید</p></a>
                <br>
                <hr>
                <form method="post" action="{{route('admin.user.import.save')}}" enctype="multipart/form-data">
                    @csrf
                    <input type="file" name="file">
                    <br>
                    <button type="submit" class="btn btn-success horizontal-form-button">ذخیره اطلاعات</button>
                </form>
            </div>
        </div>
    </div>
@endsection
