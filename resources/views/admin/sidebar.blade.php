<div class="page-sidebar">
    <a class="logo-box" href="/"><span>پنل مدیریت</span><i class="icon-radio_button_unchecked"
                                                           id="fixed-sidebar-toggle-button"></i>
        <i class="icon-close" id="sidebar-toggle-button-close"></i></a>
    <div class="page-sidebar-inner">
        <div class="page-sidebar-menu">
            <ul class="accordion-menu">

                <li @if(request()->route()->getName() == 'home') class="active-page" @endif>
                    <a href="{{route('home')}}">
                        <i class="menu-icon icon-home4"></i><span>صفحه اصلی</span>
                    </a>
                </li>

                @can('read-users')
                <li @if(request()->route()->getName() == 'admin.user.index') class="active-page" @endif>
                    <a href="{{route('admin.user.index')}}">
                        <i class="menu-icon icon-users"></i><span>مدیریت کاربران</span>
                    </a>
                </li>
                @endcan

                @can('read-admins')
                    <li @if(request()->route()->getName() == 'admin.admin.index') class="active-page" @endif>
                        <a href="{{route('admin.admin.index')}}">
                            <i class="menu-icon icon-users"></i><span>مدیریت پرسنل</span>
                        </a>
                    </li>
                @endcan

                @can('read-contents')
                    <li @if(request()->route()->getName() == 'admin.category.index') class="active-page" @endif>
                        <a href="{{route('admin.category.index')}}">
                            <i class="menu-icon icon-books"></i><span>مدیریت دوره ها</span>
                        </a>
                    </li>

                    <li @if(request()->route()->getName() == 'admin.lesson.index') class="active-page" @endif>
                        <a href="{{route('admin.lesson.index')}}">
                            <i class="menu-icon icon-book"></i><span>مدیریت دروس</span>
                        </a>
                    </li>
                    <li @if(request()->route()->getName() == 'admin.package.index') class="active-page" @endif>
                        <a href="{{route('admin.package.index')}}">
                            <i class="menu-icon icon-folder-open"></i><span>مدیریت پکیج ها</span>
                        </a>
                    </li>
                @endcan

                @can('read-transactions')
                    <li @if(request()->route()->getName() == 'admin.transaction.index') class="active-page" @endif>
                        <a href="{{route('admin.transaction.index')}}">
                            <i class="menu-icon icon-barcode"></i><span>مدیریت تراکنش ها</span>
                        </a>
                    </li>
                @endcan

                @can('read-transactions')
                    <li @if(request()->route()->getName() == 'admin.userlesson.index') class="active-page" @endif>
                        <a href="{{route('admin.userlesson.index')}}">
                            <i class="menu-icon icon-attach_money"></i><span>مدیریت خرید ها</span>
                        </a>
                    </li>
                @endcan

                @can('read-coupons')
                    <li @if(request()->route()->getName() == 'admin.coupon.index') class="active-page" @endif>
                        <a href="{{route('admin.coupon.index')}}">
                            <i class="menu-icon icon-price-tags"></i><span>مدیریت کدتخفیف ها</span>
                        </a>
                    </li>
                @endcan

                @can('read-statics')
                    <li @if(request()->route()->getName() == 'admin.static.index') class="active-page" @endif>
                        <a href="{{route('admin.static.index')}}">
                            <i class="menu-icon icon-files-empty"></i><span>مدیریت صفحه ثابت ها</span>
                        </a>
                    </li>
                @endcan

                @can('read-sliders')
                    <li @if(request()->route()->getName() == 'admin.slider.index') class="active-page" @endif>
                        <a href="{{route('admin.slider.index')}}">
                            <i class="menu-icon icon-file-picture"></i><span>مدیریت اسلایدر ها</span>
                        </a>
                    </li>
                @endcan
                @can('read-quizzes')
                    <li @if(request()->route()->getName() == 'admin.quiz.index') class="active-page" @endif>
                        <a href="{{route('admin.quiz.index')}}">
                            <i class="menu-icon icon-display"></i><span>گزارش آزمون ها</span>
                        </a>
                    </li>
                @endcan

                @can('read-notifications')
                    <li @if(request()->route()->getName() == 'admin.notification.index') class="active-page" @endif>
                        <a href="{{route('admin.notification.index')}}">
                            <i class="menu-icon icon-bubbles"></i><span>مدیریت نوتیفیکیشن</span>
                        </a>
                    </li>
                @endcan


            </ul>
        </div>
    </div>
</div>
