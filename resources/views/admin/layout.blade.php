<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Responsive Admin Dashboard Template">
    <meta name="keywords" content="admin,dashboard">
    <meta name="author" content="stacks">
    <title>@yield('title','پنل مدیریت')</title>
    <link href="{{asset('admin/assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('admin/assets/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('admin/assets/plugins/icomoon/style.css')}}" rel="stylesheet">
    <link href="{{asset('admin/assets/plugins/waves/waves.min.css')}}" rel="stylesheet">
    <link href="{{asset('admin/assets/plugins/uniform/css/default.css')}}" rel="stylesheet">
    <link href="{{asset('admin/assets/plugins/switchery/switchery.min.css')}}" rel="stylesheet"/>
    <link href="{{asset('admin/assets/plugins/nvd3/nv.d3.min.css')}}" rel="stylesheet">
    <link href="{{asset('admin/assets/plugins/toastr/toastr.min.css')}}" rel="stylesheet">
    <link href="{{asset('admin/assets/plugins/datatables/css/jquery.datatables.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('admin/assets/plugins/datatables/css/jquery.datatables_themeroller.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('admin/assets/plugins/summernote-master/summernote.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('admin/assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('admin/assets/css/persianDatepicker-default.css')}}" rel="stylesheet" type="text/css"/>

    <!-- Theme Styles -->
    <link href="{{asset('admin/assets/css/metrotheme.min.css')}}" rel="stylesheet">
    @yield('css')
    <link href="{{asset('admin/assets/css/custom.css')}}" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <![endif]-->
</head>
<body>

<!-- Page Container -->
<div class="page-container">
    <!-- Page Sidebar -->
        @include('admin.sidebar')
    <!-- /Page Sidebar -->

    <!-- Page Content -->
    <div class="page-content">

        <!-- Page Header -->
        @include('admin.header')
        <!-- Page Inner -->
        <div class="page-inner no-page-title">

            <div id="main-wrapper">
                @yield('content')
            </div><!-- Main Wrapper -->


            <div class="page-footer">

            </div>
        </div><!-- /Page Inner -->
    </div><!-- /Page Content -->
</div><!-- /Page Container -->


<!-- Javascripts -->
<script src="{{asset('admin/assets/plugins/jquery/jquery-3.1.0.min.js')}}"></script>
<script src="{{asset('admin//assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('admin/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<script src="{{asset('admin/assets/plugins/waves/waves.min.js')}}"></script>
<script src="{{asset('admin/assets/plugins/uniform/js/jquery.uniform.standalone.js')}}"></script>
<script src="{{asset('admin/assets/plugins/switchery/switchery.min.js')}}"></script>
<script src="{{asset('admin/assets/plugins/d3/d3.min.js')}}"></script>
<script src="{{asset('admin/assets/plugins/nvd3/nv.d3.min.js')}}"></script>
<script src="{{asset('admin/assets/plugins/flot/jquery.flot.min.js')}}"></script>
<script src="{{asset('admin/assets/plugins/flot/jquery.flot.time.min.js')}}"></script>
<script src="{{asset('admin/assets/plugins/flot/jquery.flot.symbol.min.js')}}"></script>
<script src="{{asset('admin/assets/plugins/flot/jquery.flot.resize.min.js')}}"></script>
<script src="{{asset('admin/assets/plugins/flot/jquery.flot.tooltip.min.js')}}"></script>
<script src="{{asset('admin/assets/plugins/flot/jquery.flot.pie.min.js')}}"></script>
<script src="{{asset('admin/assets/plugins/chartjs/chart.min.js')}}"></script>
<script src="{{asset('admin/assets/js/metrotheme.min.js')}}"></script>
<script src="{{asset('admin/assets/plugins/toastr/toastr.min.js')}}"></script>
<script src="{{asset('admin/assets/plugins/datatables/js/jquery.datatables.min.js')}}"></script>
<script src="{{asset('admin/assets/plugins/summernote-master/summernote.min.js')}}"></script>
<script src="{{asset('admin/assets/plugins/select2/js/select2.min.js')}}"></script>
<script src="{{asset('admin/assets/plugins/swal/js/swal.min.js')}}"></script>
<script src="{{asset('admin/assets/js/persianDatepicker.js')}}"></script>
<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
    $(".datePicker").persianDatepicker();
</script>
<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>

@include('admin.notyalerts')
@yield('script')
</body>
</html>
