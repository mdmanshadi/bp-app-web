<!doctype html>
<html lang="fa">
<style>
    html{
        direction: rtl;
    }
    @font-face {
        font-family: IRANYekan;
        font-style: normal;
        src: url('{{asset('admin/assets/')}}/fonts/IRANYekan.woff2') format('woff2'), url('{{asset('admin/assets/')}}/fonts/IRANYekan.woff') format('woff'), url('{{asset('admin/assets/')}}/fonts/IRANYekan.ttf') format('ttf');
    }
    body{
        font-family: IRANYekan,tahoma !important;
        font-size: 14px;
        line-height: 2;
        padding: 15px;
    }
</style>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body>
<p>
    {!! $content !!}
</p>
</body>
</html>