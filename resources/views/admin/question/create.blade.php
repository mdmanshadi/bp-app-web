@extends('admin.layout')
@section('content')
    <form class="form-horizontal" method="post" action="{{route('admin.question.save')}}" enctype="multipart/form-data">

    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">سوال</h4>
            </div>
            <div class="panel-body" data-select2-id="8">

                    {{csrf_field()}}
                    @if(!empty($question))
                        <input type="hidden" name="id" value="{{$question->id}}">
                    @endif
                <input type="hidden" name="lesson_id" value="{{$lesson->id}}">

                <div class="form-group">
                        <label for="input-Default" class="col-sm-2 control-label">سوال</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="title" value="{{$question->title ?? ''}}">
                        </div>
                    </div>

                    <div class="form-group" data-select2-id="6">
                        <label class="col-sm-2 control-label">سطح</label>
                        <div class="col-sm-10">
                            <select class="form-control select2" name="level">
                                <option value="0" @if(($question->level ?? '') == 0) selected @endif>انتخاب کنید</option>
                                <option value="1" @if(($question->level ?? '') == 1) selected @endif>آسان</option>
                                <option value="2" @if(($question->level ?? '') == 2) selected @endif>متوسط</option>
                                <option value="3" @if(($question->level ?? '') == 3) selected @endif>سخت</option>
                            </select>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-success horizontal-form-button">ذخیره اطلاعات</button>
                        </div>
                    </div>

            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">تعریف پاسخ ها  </h4><h4><i class="fa fa-plus add-new"></i></h4>
            </div>
            <div class="panel-body answers">
                <div class="answer-block row" @if(!empty($answers)) style="display: none;" @endif>
                    <div class="col-md-1" style="padding-top: 10px">
                        <input type="hidden" name="is_true[1]" value="0">
                        <input type="checkbox" class="form-control" name="is_true[1]">
                    </div>
                    <div class="col-md-10">
                        <input type="text" class="form-control" name="answer[1]" value="">
                    </div>
                    <div class="col-md-1" style="padding-top: 10px">

                    </div>
                </div>
                @if(!empty($answers))
                    @foreach($answers as $answer)
                        <div class="answer-block row">
                            <div class="col-md-1" style="padding-top: 10px">
                                <input type="hidden" name="is_true[{{$answer->id}}]" value="0">
                                <input type="checkbox" class="form-control" name="is_true[{{$answer->id}}]" @if($answer->is_true == '1') checked @endif>
                            </div>
                            <div class="col-md-10">
                                <input type="text" class="form-control" name="answer[{{$answer->id}}]" value="{{$answer->value}}">
                            </div>
                            <div class="col-md-1" style="padding-top: 10px">

                            </div>
                        </div>
                    @endforeach
                @endif

            </div>
        </div>
    </div>
    </form>
    <style>
        .answer-block{
            margin-bottom: 15px;
        }
    </style>
@endsection
@section('script')
    <script>
        var ii = @if(!empty($answers)) {{$answer->id}} @else 2 @endif;
        $(".add-new").on("click",function (e) {
            ii = ii +1;
            var content = '<div class="answer-block row">\n' +
                '                    <div class="col-md-1" style="padding-top: 10px">\n' +
                '                        <input type="hidden" name="is_true['+ii+']" value="0">\n' +
                '                        <input type="checkbox" class="form-control" name="is_true['+ii+']">\n' +
                '                    </div>\n' +
                '                    <div class="col-md-10">\n' +
                '                        <input type="text" class="form-control" name="answer['+ii+']" value="">\n' +
                '                    </div>\n' +
                '                    <div class="col-md-1" style="padding-top: 10px">\n' +
                '\n' +
                '                    </div>\n' +
                '                </div>'
            $(".answers").append('<div class="answer-block row">'+content+'</div>')

        })
    </script>
@endsection