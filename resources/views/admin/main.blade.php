@extends('admin.layout')
@section('content')
    <style>
        .mainpage p{
            font-size: 23px;
            margin: 0px;
        }
        .mainpage .panel-title {
            font-size: 14px;
            padding-bottom: 15px;
        }
        .mainpage .icon{
            font-size: 70px;
            float: left;
            margin-top: -50px;
            margin-bottom: -50px;
            color: #ec5e69;
        }
    </style>
    <div class="row mainpage">
        <div class="col-md-3">
            <div class="panel panel-white">
                <div class="panel-heading clearfix">
                    <h4 class="panel-title">تعداد کل مشتریان</h4>
                </div>
                <div class="panel-body">
                    <div class="icon">
                        <i class="fa fa-user"></i>
                    </div>
                    <p>{{number_format(\App\User::count())}} نفر</p>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="panel panel-white">
                <div class="panel-heading clearfix">
                    <h4 class="panel-title">تعداد کل رانندگان</h4>
                </div>
                <div class="panel-body">
                    <div class="icon">
                        <i class="fa fa-cab"></i>
                    </div>
                    <p>{{number_format(\App\Driver::count())}} نفر</p>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="panel panel-white">
                <div class="panel-heading clearfix">
                    <h4 class="panel-title">تعداد کل سرویس ها</h4>
                </div>
                <div class="panel-body">
                    <div class="icon">
                        <i class="fa fa-address-book"></i>
                    </div>
                    <p>{{number_format(\App\Service::count())}} عدد</p>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="panel panel-white">
                <div class="panel-heading clearfix">
                    <h4 class="panel-title">تعداد کل سفر ها</h4>
                </div>
                <div class="panel-body">
                    <div class="icon">
                        <i class="fa fa-credit-card"></i>
                    </div>
                    <p>{{number_format(\App\Trip::count())}} عدد</p>
                </div>
            </div>
        </div>
    </div>

@endsection
