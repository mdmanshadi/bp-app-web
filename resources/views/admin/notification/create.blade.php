@extends('admin.layout')
@section('content')
    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">ارسال نوتیفیکیشن</h4>
            </div>
            <div class="panel-body">
                <form class="form-horizontal" method="post" action="{{route('admin.notification.save')}}" enctype="multipart/form-data">
                    @csrf

                    <div class="form-group">
                        <label for="input-Default" class="col-sm-2 control-label">عنوان</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="title" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="input-Default" class="col-sm-2 control-label">متن کوچک</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="description" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="input-Default" class="col-sm-2 control-label">متن کامل پیام</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="full_description" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-success horizontal-form-button">ارسال نوتیفیکیشن</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
