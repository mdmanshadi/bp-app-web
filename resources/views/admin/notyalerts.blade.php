@if ($errors->any())
    <?php
    $data = '';
        foreach ($errors->all() as $error){
            $data .=$error.'</br>';
        }

        ?>
    <script>
        toastr["error"]("", "{!! $data !!}")
    </script>
@endif
@if ( Session::has('success') )
    <script>

        toastr["success"]("", "{!! Session::get('success') !!}")
    </script>
@endif

@if ( Session::has('warning') )
    <script>
        toastr["error"]("", "{!! Session::get('warning') !!}")
    </script>
@endif

@if ( Session::has('danger') )
    <script>
        toastr["error"]("", "{!! Session::get('danger') !!}")
    </script>
@endif

@if ( Session::has('info') )
    <script>
        toastr["error"]("", "{!! Session::get('info') !!}")
    </script>
@endif
<script>

toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-top-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut",
    "escapeHtml": true
}
</script>