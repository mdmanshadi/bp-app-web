<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Title -->
    <title>ورود به سامانه</title>

    <!-- Styles -->
    <link href="{{asset('admin/assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('admin/assets/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('admin/assets/plugins/icomoon/style.css')}}" rel="stylesheet">
    <link href="{{asset('admin/assets/plugins/waves/waves.min.css')}}" rel="stylesheet">
    <link href="{{asset('admin/assets/plugins/uniform/css/default.css')}}" rel="stylesheet">
    <link href="{{asset('admin/assets/plugins/toastr/toastr.min.css')}}" rel="stylesheet">

    <!-- Theme Styles -->
    <link href="{{asset('admin/assets/css/metrotheme.min.css')}}" rel="stylesheet">
    <link href="{{asset('admin/assets/css/custom.css')}}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<!-- Page Container -->
<div class="page-container login-page">

    <!-- Page Content -->
    <div class="page-content">

        <!-- Page Inner -->
        <div class="page-inner">
            <div id="main-wrapper"><div class="row">
                    <div class="col-md-3 col-login-box-alt">
                        <div class="panel panel-white login-box">
                            <div class="panel-body">
                                <a href="index.html" class="logo-name">ورود به سامانه</a>
                                <p class="m-t-md"></p>
                                <form class="m-t-md" action="{{route('login')}}" method="post">
                                    @csrf
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="username" placeholder="نام کاربری" required="">
                                    </div>
                                    <div class="form-group">
                                        <input type="password" name="password" class="password form-control" placeholder="رمزعبور" required="">
                                    </div>
                                    <button type="submit" class="btn btn-success btn-block">ورود به سامانه</button>

                                </form>
                                <p class="text-center m-t-xs text-sm login-footer"></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- Main Wrapper -->
        </div><!-- /Page Inner -->
    </div><!-- /Page Content -->
</div><!-- /Page Container -->


<!-- Javascripts -->
<script src="{{asset('admin/assets/plugins/jquery/jquery-3.1.0.min.js')}}"></script>
<script src="{{asset('admin/assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('admin/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<script src="{{asset('admin/assets/plugins/waves/waves.min.js')}}"></script>
<script src="{{asset('admin/assets/plugins/uniform/js/jquery.uniform.standalone.js')}}"></script>
<script src="{{asset('admin/assets/js/metrotheme.min.js')}}"></script>
<script src="{{asset('admin/assets/plugins/toastr/toastr.min.js')}}"></script>
@include('admin.notyalerts')
<script>

</script>
</body>

</html>
