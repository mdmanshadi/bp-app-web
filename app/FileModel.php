<?php

namespace App;

class FileModel extends MainModel
{
    //
    protected $table = 'files';

    public function lesson()
    {
        return $this->belongsTo("App\Lesson","lesson_id");
    }

}
