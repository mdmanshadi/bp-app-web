<?php

namespace App;

use Carbon\Carbon;

class Quiz extends MainModel
{
    //
    protected $table = 'quiz';

    public function user()
    {
        return $this->belongsTo("App\User","user_id");

    }
    public function getTypeTextAttribute()
    {
        //دریافت نوع خرید
        switch ($this->type){
            case 'course':
                return " دوره";
                break;
            case 'package':
                return " پکیج";
                break;
            case 'lesson':
                return " درس";
                break;
        }
    }

    public function getuserNameAttribute()
    {
        return $this->user->name ?? '-';
    }
    public function getTitleAttribute()
    {
        //دریافت عنوان بر اساس نوع خرید
        if($this->type == 'course')
            return Category::find($this->rel_id)->title ?? '-';
        elseif($this->type == 'package')
            return Package::find($this->rel_id)->title ?? '-';
        elseif($this->type == 'lesson')
            return Lesson::find($this->rel_id)->title ?? '-';
    }

    public function getFullTitleAttribute()
    {
        return $this->TypeText.' '.$this->Title;
    }
    public function getFaCreatedAtAttribute()
    {
        return jdate($this->created_at)->format("Y/m/d H:i:s");
    }
    public function getStatusTextAttribute()
    {
        if($this->status_id == '2')
            return "مردود";
        elseif($this->status_id == '1')
            return "قبول";
        else
            return  "در حال برگزاری";
    }
}
