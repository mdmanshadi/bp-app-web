<?php

namespace App;

class User extends MainModel
{
    //
    protected $table = 'users';
    protected $guarded = [];
    protected $casts = [
        'b_y' => 'string',
        'b_m' => 'string',
        'b_d' => 'string',
        'r_s' => 'string',
        'r_m' => 'string'
    ];
}
