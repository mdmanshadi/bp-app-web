<?php

namespace App;

class Coupon extends MainModel
{
    //
    protected $table = 'coupons';

    public function getUsedAttribute()
    {
        //تعداد استفاده شده
        return Transaction::where("coupon_id",$this->id)->count();
    }

    public function canUse($user_id)
    {
        //تعداد استفاه شده در خرید ها و بررسی با مقدار کد تخفیف
        $use = Transaction::where("coupon_id",$this->id);
        if($use->count() > $this->max_use)
            return false;

        //بررسی تعداد خریداری شده توسط کاربر و برررسی با مقدار کد تخفیف
        $use = Transaction::where("coupon_id",$this->id)->where("user_id",$user_id);
        if($use->count() > $this->max_use_per_user)
            return false;

        //بررسی بازه زمانی که کد تخفیف فعال هست
        if(!empty($this->start_at) && !empty($this->expire_at)) {
            //تبدیل تاریخ به carbon
            $start = Helper::toDateTime($this->start_at);
            $end = Helper::toDateTime($this->expire_at);
            if ($start->isPast() && $end->isFuture()) {
                return true;
            }
        }
        return false;
    }

}
