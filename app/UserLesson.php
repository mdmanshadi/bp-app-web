<?php

namespace App;

class UserLesson extends MainModel
{
    //
    protected $table = 'users_lessons';
    protected $guarded =[];

    public function getTitleAttribute()
    {
        //دریافت عنوان بر اساس نوع خرید
        if($this->type == 'course')
            return Category::find($this->rel_id)->title ?? '-';
        elseif($this->type == 'package')
            return Package::find($this->rel_id)->title ?? '-';
        elseif($this->type == 'lesson')
            return Lesson::find($this->rel_id)->title ?? '-';
    }
    public function user()
    {
        return $this->belongsTo("App\User","user_id");
    }

    public function getuserNameAttribute()
    {
        return $this->user->name ?? '-';
    }

    public function getTypeTextAttribute()
    {
        //دریافت نوع خرید
        switch ($this->type){
            case 'course':
                return " دوره";
                break;
            case 'package':
                return " پکیج";
                break;
            case 'lesson':
                return " درس";
                break;
        }
    }
    public function getFaCreatedAtAttribute()
    {
        return jdate($this->created_at)->format("Y/m/d H:i:s");
    }
    public function getTransactionPayedAttribute()
    {
        return Transaction::where("user_id",$this->user_id)->where("type",$this->type)->where("rel_id",$this->rel_id)->sum('amount') ?? 0;
    }
    public function getTransactionLinkAttribute()
    {
        return "<a href='".route('admin.transaction.index',['id'=>$this->transaction_id])."'>".$this->transaction_id."</a>";
    }

}
