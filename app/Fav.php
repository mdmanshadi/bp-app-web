<?php

namespace App;

class Fav extends MainModel
{
    //
    protected $table = 'favs';


    public function user()
    {
        return $this->belongsTo("App\User","user_id");
    }
    public function rel()
    {
        if($this->type == 'lesson'){
            return $this->belongsTo(Lesson::class,"rel_id");
        }elseif($this->type == 'category'){
            return $this->belongsTo(Category::class,"rel_id");
        }elseif($this->type == 'package'){
            return $this->belongsTo(Package::class,"rel_id");
        }
    }

}
