<?php

namespace App;

class Category extends MainModel
{
    //
    protected $table = 'categories';

    public function getparentTitleAttribute()
    {
        return $this->parent->title ?? '-';
    }

    public function parent()
    {
        return $this->belongsTo("App\Category","parent_id");
    }
    public function getPicAttribute()
    {
        return empty($this->attributes['pic']) ? 'no-image-n.png' : $this->attributes['pic'];
    }

    public function lessons()
    {
        return $this->hasMany(Lesson::class,'category_id','id');
    }

}
