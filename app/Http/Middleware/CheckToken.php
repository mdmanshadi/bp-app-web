<?php

namespace App\Http\Middleware;

use App\Token;
use Closure;

class CheckToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Token::FindUser($request->header('Authorization'));
        if(empty($user))
            return ['result'=>'failed','message'=>'برای تماشای این کلیپ باید وارد حساب کاربری تان شود'];
        return $next($request);
    }
}
