<?php

namespace App\Http\Controllers;

use App\Category;
use App\Notification;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    var $title = '';
    var $newRoute = '';
    var $colsNames = '';
    var $cols = '';
    var $actions = '';
    var $rows = '';
    var $saveRoute = '';
    var $mainRoute = '';
    var $fields = [];
    var $model = 'Notification';

    public function __construct()
    {
        $this->newRoute = 'admin.notification.create';
        $this->mainRoute = 'admin.notification.index';
        $this->colsNames = ["عنوان","متن خلاصه","متن پیام","زمان ارسال"];
        $this->cols = ["title","description","full_description","FaCreatedAt"];
        $this->actions = [
            ["route"=>'admin.notification.delete',"icon"=>'fa fa-trash','caption'=>"حذف","type"=>"ask"],
        ];
    }
    public function create(Request $request)
    {
        return view('admin.notification.create');
    }

    public function save(Request $request)
    {

        $this->validate($request,[
           'title'=>'required',
           'description'=>'required',
        ]);

        $appId = '6g033wm7rqynmkkg';
        $token = 'd9ebfaed156e77ccbff892821089f2c8eb81fedb';

        $TOKEN = $token;
        $notification = new Notification();
        $notification->title = $request->input('title');
        $notification->description = $request->input('description');
        $notification->full_description = $request->input('full_description');
        $notification->save();

        $data = array(
            "app_ids" => [$appId],
            "data" => array(
                "title" => $request->input('title'),
                "content" => $request->input('description'),
                "big_content" => $request->input('full_description') ?? $request->input('description'),
                "wake_screen"=>true,
                "action_type"=>"G",
                "platform"=>"1",

            ),
        );
        $ch = curl_init("https://api.pushe.co/v2/messaging/notifications/");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/json",
            "Accept: application/json",
            "Authorization: Token " . $TOKEN,
        ));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        $response = curl_exec($ch);
        $hashed_id = json_decode($response)->hashed_id;

        curl_close($ch);
        return redirect()->route('admin.notification.index')->with("success","ارسال با موفقیت انجام شد.");
    }
}
