<?php

namespace App\Http\Controllers;

use App\Category;
use App\Fav;
use App\FileModel;
use App\Helper;
use App\Lesson;
use App\Package;
use App\Quiz;
use App\Rate;
use App\Slider;
use App\Token;
use App\User;
use Str;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Ipecompany\Smsirlaravel\Smsirlaravel;

class NewApiController extends Controller
{
//    private $user = '';

//    public function __construct(Request $request)
//    {
//        $token = $request->header('Authorization');
//        if (isset($token)) {
//            $this->user = Token::FindUser($token);
//        }
//    }

    public function verifyToken(Request $request)
    {
        $token = $request->token;
        $user = Token::FindUser($token);
        return ['message' => 'valid token', 'user' => $user];
    }

    public function login(Request $request)
    {
        //تابع ورود به سیستم
        $username = $request->input('username');
        $code = $request->input('code');
        $user = User::where("username", $username);
        if ($user->count() == 0) {
            //اگر یوزر وجود نداره باید ثبت نام صورت بگیره و تلفن همراه اعتبار سنجی بشه
            if (Helper::validateMobile($username)) {
                //ساخت یوزر جدید در دیتابیس
                $user = new User();
                $user->name = '';
                $user->username = $username;
                $user->password = bcrypt($username . rand(1, 1000));
                $user->credit = 0;
                $user->status_id = 1;
                $user->ref_code = $request->input('ref_code');
                //تولید کد فعال سازی یکتا به صورت رندوم
                $user->sms_verify_code = rand(1000, 9999);
                $user->save();
                //ارسال اس ام اس فعال سازی
                Smsirlaravel::sendVerification($user->sms_verify_code, $user->username);
                // اگر مقدار new برای اپ ارسال بشه اپ متوجه میشه که فرآیند برای ثبت نام هست و از کاربر درخواست نام و نام خانوادگی و اطلاعات ثبت نام را میکنه
                return "new";
            } else throw new \Exception("تلفن همراه وارد شده صحیح نیست");

        } else $user = $user->get()->first();
        //در صورت وجود داشتن کاربر عملیات ورود به سیستم انجام خواهد شد
        //بررسی این که کاربر مسدود نباشد
        if ($user->status_id != '1')
            throw new \Exception("حساب کاربری شما مسدود شده است");
        if (empty($code)) {
            //اگر هنوز کاربر کد را ارسال نکرده باشد // یعنی ثبت نام در مرحله وارد کردن شماره موبایل باشد کد فعال سازی ساخته شده و ارسال میشه
            $user->sms_verify_code = rand(1000, 9999);
            $user->save();
            Smsirlaravel::sendVerification($user->sms_verify_code, $user->username);
            //اگر به هر دلیلی نام کاربر خالی بود (چه تازه ثبت نام کرده و یا اگر نام خودشو خالی گذاشته با کاربر مشابه کاربر جدید برخورد میشه و درخواست میشه اطلاعاتش رو کامل وارد کنه
            if (empty($user->name))
                return "new";
            else return "1";

        } else {
            //درصورتی که عملیات در مرحله دوم یعنی ورود کد باشه کد بررسی شده و در صورت صحیح بودن وارد سامانه میشه
            if ($user->sms_verify_code != $code)
                throw new \Exception("کد وارد شده صحیح نیست");
            if (empty($user->name)) {
                $user->name = $request->input('name');
                $user->save();
            }

            //بررسی اینکه در جدول توکن ها این یوزر قبلا وجود داشته یا نه
            $token = Token::where("rel", "user")->where("rel_id", $user->id);
            if ($token->count() > 0)
                $token = $token->get()->first();
            else
                $token = new Token();
            // در صورت وجود نداشتن ساخت یک توکن جدید

            $token->rel = "user";
            $token->rel_id = $user->id;
            //ساخت یک رشته کد رندوم ۱۲۰ کاراکتری برای توکن کاربران
            $token->user_token = Str::random(120);
            $token->save();
            return ['user' => $user, 'token' => $token->user_token];
        }
    }

    public function main()
    {
        $return = [];
//        $return['user'] = $this->user;

//      sliders
        $sliders = Slider::where("status_id", "1")->get();
        foreach ($sliders as $slider) {
            $slider->pic = Storage::url($slider->pic);
        }
        $return['sliders'] = $sliders;

//      packages
        $packages = Package::where("status_id", "1")->select("id", "title", "pic", "price", "discount", "score", "for_sale")->get();
        foreach ($packages as $package) {
            $package->pic = Storage::url($package->pic);
        }
        $return['packages'] = $packages;

//        courses
        $courses = Category::where("status_id", "1")->select("id", "title", "pic", "price", "discount", "for_sale", "score")->orderBy('created_at')->get();
        foreach ($courses as $course) {
            $course->pic = Storage::url($course->pic);
        }
        $return['courses'] = $courses;

//        files
        $files = FileModel::where("type", "video")->select("id", "title", "lesson_id")->orderBy('created_at')->limit(8)->get();
        foreach ($files as $file) {
            $file_picture = $file->lesson['pic'];
            if ($file_picture !== 'no-image-n.png' && $file_picture !== null) {
                $file->pic = Storage::url($file_picture);
            } else {
                $file->pic = '';
            }
        }
        $return['files'] = $files;
        return $return;
    }

    public function getCourseDetails($id)
    {
        $cat = Category::findOrfail($id);
        $cat->pic = Storage::url($cat->pic);
        $cat->description = ($cat->description);
        $cat->lessons = Lesson::where("category_id", $cat->id)->select('title', 'id', 'description')->get();
//        $cat->cats = Category::where("parent_id", $cat->id)->select('title', 'id', 'description')->get();

//        $cat->owned = (Helper::isPurchased("course", $cat->id, $user->id)) ? '1' : '0';
//        $cat->stars = Rate::where("type","course")->where("rel_id",$cat->id)->where("user_id",$user->id)->first()->star ?? 0;
//        $cat->isFav = Fav::where("user_id",$user->id)->where("type","category")->where("rel_id",$cat->id)->count() > 0 ? '1' : '0';
        return $cat;
    }

    public function getLessonDetails($id)
    {
        $lesson = Lesson::findOrfail($id);
        $lesson->pic = Storage::url($lesson->pic);
        $lesson->description = $lesson->description;
        $files = FileModel::where("lesson_id", $lesson->id)->select("title", "id", "type")->get();
//        $lesson->stars = Rate::where("type","lesson")->where("rel_id",$lesson->id)->where("user_id",$user->id)->first()->star ?? 0;

//        //لیست فایل ها و نوعشون
//        foreach ($files as $file) {
//            $type = '-';
//
//            if($file->type == 'video' || $file->type == 'audio'){
//                $type = 'فیلم';
//                $file->type = 'video';
//            }elseif ($file->type == 'pdf'){
//                $type = 'پی دی اف';
//            }elseif ($file->type == 'other'){
//                $type = 'فایل';
//            }
//            $file->file_type = $type;
//
//        }
        $lesson->files = $files;
//        $lesson->owned = (Helper::isPurchased("lesson",$lesson->id,$user->id)) ? '1':'0';
//        $lesson->isFav = Fav::where("user_id",$user->id)->where("type","lesson")->where("rel_id",$lesson->id)->count() > 0 ? '1' : '0';

//        $lesson->hasExam = 0;
//        if($lesson->owned) {
//            //اگر آزمون برای این دوره فعال باشد و کاربر این دوره را خریده باشد این گزینه ۱ هست
//            if(empty($lesson->exam_detail))
//                $lesson->hasExam = 0;
//            else{
//                $exam_detail = json_decode($lesson->exam_detail);
//                if(($exam_detail->status_id ?? 0) != '1')
//                    $lesson->hasExam = 0;
//                else {
//                    $lesson->hasExam = 1;
//                    $lesson->ExamDetail = json_decode($lesson->exam_detail);
//                    $lesson->quizScore = 0;
//                    $lesson->quizStatus = 0;
//                    //check exam history
//                    $quiz = Quiz::where("user_id", $user->id)->where("type", "course")->where("rel_id", $id)->where("status_id", "1")->orderBy("created_at", "desc")->get()->first();
//                    if (!empty($quiz)) {
//                        $lesson->quizScore = $quiz->score;
//                        $lesson->quizStatus = $quiz->status_id;
//                    }
//                }
//            }
//        }
        return $lesson;
    }

    public function getFileDetails($id)
    {
        $file = FileModel::find($id);
        $file->file_url = Storage::url($file->file);
        return $file;
    }

    public function getPackageDetails($id)
    {
        $cat = Package::findOrfail($id);
        $cat->pic = Storage::url($cat->pic);
        $cat->description = ($cat->description);
        $categories = json_decode($cat->categories);
        $categories = is_array($categories) ? $categories : [];
        $cat->lessons = Category::whereIn("id", $categories)->get();
        return $cat;
    }
}
