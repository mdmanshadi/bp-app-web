<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class QuizController extends Controller
{
    var $title = '';
    var $newRoute = '';
    var $colsNames = '';
    var $cols = '';
    var $actions = '';
    var $rows = '';
    var $saveRoute = '';
    var $mainRoute = '';
    var $fields = [];
    var $model = 'Quiz';

    public function __construct()
    {
        $request = Request::capture();
        $this->title = 'آزمون';
        $this->mainRoute = 'admin.quiz.index';
        $this->colsNames = ["کاربر شرکت کننده","آزمون","وضعیت","امتیاز"];
        $this->cols = ["username","FullTitle","statusText","score"];
        $this->actions = [
            ["route"=>'admin.quiz.detail',"icon"=>'fa fa-list','caption'=>"گزارش آزمون"],
            ["route"=>'admin.quiz.delete',"icon"=>'fa fa-trash','caption'=>"حذف","type"=>"ask"],


        ];
        $this->fields = [
            ["caption"=>"عنوان","name"=>"title","valid"=>"required"],
            ["caption"=>"تصویر","name"=>"pic","type"=>"image"],
            ["caption"=>"وضعیت","name"=>"status_id","type"=>"select","valid"=>"required|in:1,2","values"=>["1"=>"فعال","2"=>"غیرفعال"]],


        ];

    }

    public function detail($id)
    {
        return redirect()->route('admin.quizquestion.index',['quiz_id'=>$id]);
    }
}
