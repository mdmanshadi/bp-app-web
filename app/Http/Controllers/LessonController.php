<?php

namespace App\Http\Controllers;

use App\Category;
use App\FileModel;
use App\Question;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class LessonController extends Controller
{
    var $title = '';
    var $newRoute = '';
    var $colsNames = '';
    var $cols = '';
    var $actions = '';
    var $rows = '';
    var $saveRoute = '';
    var $mainRoute = '';
    var $fields = [];
    var $model = 'Lesson';

    public function __construct()
    {
        $request = Request::capture();
        $this->title = 'درس';
        $this->mainRoute = 'admin.lesson.index';
        $this->newRoute = 'admin.lesson.create';
        $this->saveRoute = 'admin.lesson.save';
        $this->colsNames = ["عنوان","دسته بندی","قیمت"];
        $this->cols = ["title","CategoryTitle","priceToNFT"];
        $this->actions = [
            ["route"=>'admin.lesson.edit',"icon"=>'fa fa-pencil','caption'=>"ویرایش"],
            ["route"=>'admin.lesson.file',"icon"=>'fa fa-file','caption'=>"فایل ها"],
            ["route"=>'admin.lesson.delete',"icon"=>'fa fa-trash','caption'=>"حذف","type"=>"ask"],
            ["route"=>'admin.lesson.question',"icon"=>'fa fa-question-circle-o','caption'=>"بانک سوالات"],
            ["route"=>'admin.exam.create','params'=>["type"=>"lesson"],"icon"=>'fa fa-list','caption'=>"تنظیمات آزمون"],



        ];
        $this->fields = [
            ["caption"=>"عنوان","name"=>"title","valid"=>"required"],
            ["caption"=>"توضیحات","name"=>"description","type"=>"textarea","valid"=>"required"],
            ["caption"=>"قابل فروش ؟","name"=>"for_sale","type"=>"select","valid"=>"","values"=>["1"=>"فعال","2"=>"غیرفعال"]],
            ["caption"=>"قیمت","name"=>"price","valid"=>"required|numeric"],
            ["caption"=>"تصویر","name"=>"pic","type"=>"image"],
            ["caption"=>"تخفیف","name"=>"discount","valid"=>"numeric"],
            ["caption"=>"دسته بندی","name"=>"category_id","type"=>"select","values"=>Category::where("id","!=",$request->input('id'))->where("status_id","1")->get()->pluck('title','id')->toArray()],
            ["caption"=>"وضعیت","name"=>"status_id","type"=>"select","valid"=>"","values"=>["1"=>"فعال","2"=>"غیرفعال"]],
            ["caption"=>"هدیه خرید (تومان)","name"=>"score","valid"=>"numeric"],
        ];
        $this->searchKeys = [
            ["name"=>"title","title"=>'عنوان',"type"=>'likeInput'],
            ["name"=>"category_id","title"=>'دسته بندی',"type"=>'select',"value"=>Category::get()->pluck("title","id")->toArray()],
        ];
    }

    public function fileDelete(Request $request)
    {
        try {
            $id = $request->input('id');
            $data = FileModel::findOrfail($id);
            Storage::delete($data->file);
            $data->delete();
            return redirect()->back()->with("success",trans('validation.success'));

        }catch (\Exception $e){
            return redirect()->back()->with("danger",trans('validation.error'));
        }
    }

    public function file(Request $request,$id)
    {
        $request = Request::capture();
        $this->title = 'فایل';
        $this->mainRoute = 'admin.lesson.index';
        $this->newRoute = ['admin.lesson.newfile',["id"=>$id]];
        $this->saveRoute = 'admin.lesson.save';
        $this->colsNames = ["عنوان","نوع فایل"];
        $this->cols = ["title","file_type"];
        $this->spQuery = FileModel::where("lesson_id",$id);
        $this->actions = [
            ["route"=>'admin.lesson.editfile',"icon"=>'fa fa-pencil','caption'=>"ویرایش"],
            ["route"=>'admin.lesson.file.delete',"icon"=>'fa fa-trash','caption'=>"حذف","type"=>"ask"],

        ];

        $this->searchKeys = [
            ["name"=>"title","title"=>'عنوان',"type"=>'likeInput'],
        ];
        return parent::index($request);
    }

    public function newfile(Request $request,$id)
    {

        $this->title = 'فایل';
        $this->model = 'FileModel';

        $this->mainRoute = ['admin.lesson.file',["id"=>$id]];
        $this->newRoute = ['admin.lesson.newfile',["id"=>$id]];
        $this->saveRoute = ['admin.lesson.savefile',["id"=>$id]];
        $this->fields = [
            ["caption"=>"","name"=>"lesson_id","value"=>$id,"type"=>"hidden"],
            ["caption"=>"","name"=>"file_type","value"=>'',"type"=>"hidden",'process'=>'getType'],
            ["caption"=>"","name"=>"ext","value"=>'',"type"=>"hidden",'process'=>'getExt'],
            ["caption"=>"عنوان","name"=>"title","valid"=>"required"],
            ["caption"=>"فایل","name"=>"file","type"=>"file"],
            ["caption"=>"نوع فایل","name"=>"type","valid"=>"required|in:video,pdf,audio,other","type"=>"select","values"=>["video"=>"ویدیو","pdf"=>"پی دی اف","audio"=>"فایل صوتی","other"=>"فایل دیگر"]],
        ];
        return parent::create($request);
    }

    public function getType(Request $request)
    {
        $file = $request->file('file');
        return $file->getClientMimeType();
    }
    public function getExt(Request $request)
    {
        $file = $request->file('file');
        return $file->getClientOriginalExtension();
    }

    public function editfile($id)
    {
        $file = FileModel::findOrfail($id);
        $this->model = 'FileModel';
        $this->title = 'فایل';
        $this->mainRoute = ['admin.lesson.file',["id"=>$file->lesson_id]];
        $this->newRoute = ['admin.lesson.newfile',["id"=>$file->lesson_id]];
        $this->saveRoute = ['admin.lesson.savefile',["id"=>$file->lesson_id]];
        $this->fields = [
            ["caption"=>"","name"=>"lesson_id","value"=>$file->lesson_id,"type"=>"hidden"],
            ["caption"=>"","name"=>"file_type","value"=>'',"type"=>"hidden",'process'=>'getType'],
            ["caption"=>"","name"=>"ext","value"=>'',"type"=>"hidden",'process'=>'getExt'],
            ["caption"=>"عنوان","name"=>"title","valid"=>"required"],
            ["caption"=>"فایل","name"=>"file","type"=>"file"],
            ["caption"=>"نوع فایل","name"=>"type","valid"=>"required|in:video,pdf,audio,other","type"=>"select","values"=>["video"=>"ویدیو","pdf"=>"پی دی اف","audio"=>"فایل صوتی","other"=>"فایل دیگر"]],
            ["caption"=>"آدرس استریم","name"=>"stream"],
        ];
        return parent::edit($id);
    }

    public function savefile(Request $request,$id)
    {
        $this->model = 'FileModel';
        $this->title = 'فایل';
        $this->mainRoute = ['admin.lesson.file',["id"=>$id]];
        $this->newRoute = ['admin.lesson.newfile',["id"=>$id]];
        $this->saveRoute = ['admin.lesson.savefile',["id"=>$id]];
        $this->fields = [
            ["caption"=>"","name"=>"lesson_id","value"=>$id,"type"=>"hidden"],
            ["caption"=>"","name"=>"file_type","value"=>'',"type"=>"hidden",'process'=>'getType'],
            ["caption"=>"","name"=>"ext","value"=>'',"type"=>"hidden",'process'=>'getExt'],
            ["caption"=>"عنوان","name"=>"title","valid"=>"required"],
            ["caption"=>"فایل","name"=>"file","type"=>"file"],
            ["caption"=>"نوع فایل","name"=>"type","valid"=>"required|in:video,pdf,audio,other","type"=>"select","values"=>["video"=>"ویدیو","pdf"=>"پی دی اف","audio"=>"فایل صوتی","other"=>"فایل دیگر"]],
            ["caption"=>"آدرس استریم","name"=>"stream"],
        ];
        if(empty($request->file('file')))
            return redirect()->back()->with("danger","لطفا فایل را انتخاب کنید");
        return parent::save($request);
    }

    public function question($id)
    {
        return redirect()->route('admin.question.index',["lesson_id"=>$id]);
    }
}
