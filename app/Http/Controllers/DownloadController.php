<?php

namespace App\Http\Controllers;

use App\Category;
use App\Download;
use App\FileModel;
use App\Lesson;
use App\Package;
use App\StaticModel;
use App\Token;
use App\Transaction;
use App\User;
use App\UserLesson;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;

class DownloadController extends Controller
{
    public function dlFile(Request $request,$token)
    {
        //متد دانلود فایل
        $download = Download::where("token",$token);
        //بررسی صحت توکن ارسالی
        if($download->count() == 0)
            return "";
        $download = $download->get()->first();
        //جستجو برای فایل مورد نظر
        $file = FileModel::findOrfail($download->file_id);
        //ارسال هدر مورد نظر
        $headers = array(
            'Content-Type: '.$file->file_type,
        );
        //حذف توکن برای جلوگیری از دانلود مجدد
        $download->delete();


        //ارسال محتو برای دانلود فایل
        return Storage::download($file->file,'dlfile',$headers);


    }

    public function showStatic($id)
    {
        //نمایش وب ویو صفحات ثابت
        $content =  StaticModel::find($id)->content ?? '';
        return view('admin.webview',compact('content'));
    }
    public function PayInvoice(Request $request,$id)
    {
        //ارسال به بانک برای پرداخت آنلاین
        try {
            $transaction = Transaction::findOrfail($id);
            //جستجوی تراکنش و توکن کاربر
            $user = Token::FindUser($request->input('token'));
            if(!$user)
                throw new \Exception("کاربر صحیح نیست");
            //بررسی مالکیت تراکنش
            if ($user->id != $transaction->user_id)
                throw new \Exception("تراکنش نا معتبر");
            try {

                //فراخوانی زرین پال
                $gateway = \Gateway::zarinPal();
                $gateway->setCallback(route('callBack',['id'=>$transaction->id]));
                $gateway->price($transaction->amount *10)->ready();
                $gateway->setMobileNumber($user->username);
                $gateway->setDescription("خرید کاربر" . $user->username);
                $refId = $gateway->refId();
                $transID = $gateway->transactionId();
                //دریافت ref code و ذخیره در دیتابیس
                $transaction->ref_id = $refId;
                $transaction->res_id = $transID;
                $transaction->save();

                //ارسال برای پرداخت
                return $gateway->redirect();

            } catch (\Exception $e) {

                echo $e->getMessage();
            }
        }catch (\Exception $e){
        echo $e->getMessage();
        }
    }

    public function callBack(Request $request,$id)
    {
        //بازگشت از درگاه پرداخت
        //جستجوی تراکنش
        $transaction = Transaction::findOrfail($id);
        try {

            //اعتبار سنجی تراکنش
            $gateway = \Gateway::verify();
            $trackingCode = $gateway->trackingCode();
            $refId = $gateway->refId();
            $cardNumber = $gateway->cardNumber();
            //ذخیره تراکنش به عنوان موفق
            $transaction->status_id = '1';
            $transaction->save();
            if($transaction->type == 'wallet'){
                //اگر نوع تراکنش شارژ کیف پول باشد اعتبار کاربری شارژ میشه و کاربر به لینک مورد نظر منتقل میشه تا ادامه فرآیند در اپ صورت بگیره
                $user = User::findOrfail($transaction->user_id);
                $user->credit += $transaction->amount;
                $user->save();
                return " charged !"."<a href='OghyanooseAbi://App/3'>بازگشت به اپلیکیشن</a><script>window.location='OghyanooseAbi://App/3'</script>";

            }else {
                //در این وضعیت کاربر یک کالا رو خریده که مشخصاتش رو براش در لیست دوره های خریداری شده ذخیره میکنیم
                $userLesson = new UserLesson();
                $userLesson->user_id = $transaction->user_id;
                $userLesson->type = $transaction->type;
                $userLesson->rel_id = $transaction->rel_id;
                $userLesson->transaction_id = $transaction->id;
                $userLesson->save();
                $type = $transaction->type;

                if ($type == 'lesson') {
                    $data = Lesson::findOrfail($transaction->rel_id);
                } elseif ($type == 'course') {
                    $data = Category::findOrfail($transaction->rel_id);
                } elseif ($type == 'package') {
                    $data = Package::findOrfail($transaction->rel_id);
                }
                if (!empty($data)) {
                    // اگر دوره دارای امتیاز بعد از خرید باشه اینجا موجودی کاربر رو شارژ میکنیم
                    $prize = $data->score;
                    $user = User::findOrfail($transaction->user_id);
                    $user->credit += $prize;
                    $user->save();

                }
            }
            return " عملیات با موفقیت انجام شد و حساب کاربری شما اپدیت شد"."<a href='OghyanooseAbi://App/1'>بازگشت به اپلیکیشن</a><script>window.location='OghyanooseAbi://App/1'</script>";


        } catch (\Exception $e) {

            echo $e->getMessage()."<a href='OghyanooseAbi://App/2'>بازگشت به اپلیکیشن</a><script>window.location='OghyanooseAbi://App/2'</script>";
        }
    }

    public function success()
    {
        //در صورت صحیح بودن عملیات پرداخت ادامه فرآیند رو به اپلیکیشن منتقل میکنیم
        return " عملیات با موفقیت انجام شد و حساب کاربری شما اپدیت شد"."<a href='OghyanooseAbi://App/1'>بازگشت به اپلیکیشن</a><script>window.location='OghyanooseAbi://App/1'</script>";

    }
}
