<?php

namespace App\Http\Controllers;

use App\Category;
use App\User;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    var $title = '';
    var $newRoute = '';
    var $colsNames = '';
    var $cols = '';
    var $actions = '';
    var $rows = '';
    var $saveRoute = '';
    var $mainRoute = '';
    var $fields = [];
    var $model = 'Transaction';

    public function __construct()
    {
        $request = Request::capture();
        $this->title = 'تراکنش ها';
        $this->mainRoute = 'admin.transaction.index';
        $this->newRoute = '';
        $this->saveRoute = '';
        $this->colsNames = ["شناسه","نام کاربر","مبلغ","نوع تراکنش","وضعیت","شناسه پرداخت","شناسه پیگیری","زمان "];
        $this->cols = ["id","userName","amountToNFT","TypeText","statusText","res_id","ref_id","FaCreatedAt"];

        $this->searchKeys = [
            ["name"=>"id","title"=>'شناسه',"type"=>'input'],
            ["name"=>"ref_id","title"=>'شناسه پیگیری',"type"=>'input'],
            ["name"=>"res_id","title"=>'شناسه رسید',"type"=>'input'],
            ["name"=>"amount","title"=>'مبلغ',"type"=>'range'],
            ["name"=>"type","title"=>'نوع تراکنش',"type"=>'select',"value"=>["course"=>"خرید دوره","package"=>"خرید پکیج","lesson"=>"خرید درس","admin"=>"تغییر مدیریت","wallet"=>"شارژ کیف پول"]],
            ["name"=>"status_id","title"=>'وضعیت',"type"=>'select',"value"=>["1"=>"موفق","2"=>"ناموفق"]],
        ];
    }
}
