<?php

namespace App\Http\Controllers;

use App\Category;
use App\User;
use Illuminate\Http\Request;

class UserLessonController extends Controller
{
    var $title = '';
    var $newRoute = '';
    var $colsNames = '';
    var $cols = '';
    var $actions = '';
    var $rows = '';
    var $saveRoute = '';
    var $mainRoute = '';
    var $fields = [];
    var $model = 'UserLesson';

    public function __construct()
    {
        $request = Request::capture();
        $this->title = 'مدیریت فروش';
        $this->mainRoute = 'admin.userlesson.index';
        $this->newRoute = '';
        $this->saveRoute = '';
        $this->colsNames = ["شناسه","نام کاربر","نوع خرید","نام کالا","زمان خرید","شناسه رسید پرداخت","مبلغ پرداختی"];
        $this->cols = ["id","userName","TypeText","title","FaCreatedAt","TransactionLink","TransactionPayedToNFT"];
        $this->actions = [
            ["route"=>'admin.userlesson.delete',"icon"=>'fa fa-trash','caption'=>"حذف","type"=>"ask"],

        ];
        $this->searchKeys = [
            ["name"=>"id","title"=>'شناسه',"type"=>'input'],
            ["name"=>"type","title"=>'نوع خرید',"type"=>'select',"value"=>["course"=>" دوره","package"=>" پکیج","lesson"=>" درس"]],
            ["name"=>"created_at","title"=>'زمان خرید',"type"=>'rangedate'],
        ];
    }
}
