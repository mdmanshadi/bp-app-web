<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    var $title = '';
    var $newRoute = '';
    var $colsNames = '';
    var $cols = '';
    var $actions = '';
    var $rows = '';
    var $saveRoute = '';
    var $mainRoute = '';
    var $fields = [];
    var $model = 'Role';

    public function __construct()
    {
        $request = Request::capture();
        $this->title = 'نقش';
        $this->mainRoute = 'admin.roles.index';
        $this->newRoute = 'admin.role.create';
        $this->saveRoute = 'admin.role.save';
        $this->colsNames = ["نقش"];
        $this->cols = ["name"];
        $this->actions = [
            ["route" => 'admin.role.edit', "icon" => 'fa fa-pencil', 'caption' => "ویرایش"],
            ["route" => 'admin.role.delete', "icon" => 'fa fa-trash', 'caption' => "حذف نقش", "type" => "ask"],
        ];
        $this->fields = [
            ["route" => 'admin.role.edit', "icon" => 'fa fa-pencil', 'caption' => "ویرایش"],
            ["route" => 'admin.admin.index', "icon" => 'fa fa-user-plus', 'caption' => "مدیریت دسترسی"],
        ];
    }

    public function create(Request $request)
    {
        $permissions = $this->permissions();
        return view('admin.role.create', compact('permissions'));
    }

    public function save(Request $request)
    {
        $roleName = $request->input('name');
        $permissions = $request->input('permissions');
        if (!Role::where('name', $roleName)->exists())
            $role = Role::create(['name' => $roleName]);
        else
            $role = Role::findByName($roleName);
        $role->syncPermissions($permissions);
        return redirect()->route('admin.roles.index');
    }

    public function edit($id)
    {
        $role = Role::findById($id);
        $permissions = $this->permissions();
        $userPermissions = $role->permissions()->get()->pluck('name')->toArray();
        return view('admin.role.edit', compact('role', 'userPermissions','permissions'));
    }

    protected function permissions()
    {
        return [
            'admins' => 'پرسنل',
            'users' => 'کاربران',
            'roles' => 'نقش ها',
            'contents' => 'محتوا',
            'transactions' => 'تراکنش ها',
            'checkouts' => 'خرید ها',
            'coupons' => 'کوپن های تخفیف',
            'statics' => 'صفحه ثابت ها',
            'sliders' => 'اسلایدر ها',
            'quizzes' => 'گزارش آزمون ها',
            'notifications' => 'نوتیفیکیشن ها'
        ];
    }
}
