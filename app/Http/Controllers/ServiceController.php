<?php

namespace App\Http\Controllers;


use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Token;
use Carbon\Carbon;
use Request;
use Config;


class ServiceController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    //initial functions
    /**
     * @param string $method
     * @param array $parameters
     * @return \Illuminate\Http\JsonResponse|mixed
     * all request first meet this function
     * if any parameters fails load error file
     */
    public function __call($method, $parameters)
    {
        $request = Request::capture();
        if(!empty($parameters[0]) && !empty($parameters[1]) && !empty($parameters[2]))
            return $this->parser($parameters[2] , $parameters[0] , $parameters[1],$request);
    }

    /**
     * @param $function
     * @param $version
     * @param $category
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     * parse data | load class & create response
     */
    private function parser($function , $version , $category , $request)
    {
        $return = ["status" => "N\A","data"=>"N\A"];
        try{
            $this->CheckLogin($request);

            $class_name ='\App\ApiModule\\V'.$version.'\\'.$category.'Api';

            if(!class_exists($class_name))
                throw new \Exception("classNotFound");

            $class = new $class_name();
            if(!method_exists($class,$function))
                throw new \Exception("methodNotFound");

            $return['data'] = $class->$function($request);
            $return['status'] = 'OK';


        }catch (\Exception $e){
            $return = ["status" => "failed","data"=>$e->getMessage()];
            if(true){
                $return['errorLine'] = $e->getLine();
                $return['errorFile'] = $e->getFile();
            }

        }
        return response()->json($return, 200, array('Content-Type' => 'application/json; charset=utf-8'), JSON_UNESCAPED_UNICODE);
    }

    private function CheckLogin($request)
    {
        $token = $request->input('token');
        $platform = $request->input('platform');
        $platform = explode("|",$platform);
        $OS = $platform[1] ?? 'android';
        $App = $platform[0] ?? 'User';
        $OSVersion = $platform[2] ?? '0';

        Config::set("auth.APP",$App);
        Config::set("auth.OS",$OS);
        Config::set("auth.OS_VERSION",$OSVersion);

        try {
            $user = Token::FindUser($token);

            if(empty($user))
                return ;
            Config::set("auth.THIS_USER",$user->id);

        }catch (\Exception $e){
            return ;
        }
    }
}