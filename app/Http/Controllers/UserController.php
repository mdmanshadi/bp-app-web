<?php

namespace App\Http\Controllers;

use App\Category;
use App\Exports\UsersExport;
use App\Imports\UsersImport;
use App\Lesson;
use App\Package;
use App\Rules\JDate;
use App\Rules\Mobile;
use App\Transaction;
use App\User;
use App\UserLesson;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\HeadingRowImport;

class UserController extends Controller
{
    var $title = '';
    var $newRoute = '';
    var $colsNames = '';
    var $cols = '';
    var $actions = '';
    var $rows = '';
    var $saveRoute = '';
    var $mainRoute = '';
    var $fields = [];
    var $model = 'User';

    public function transactions($id)
    {
        return redirect()->route('admin.transaction.index', ['user_id' => $id]);
    }

    public function lessons($id)
    {
        return redirect()->route('admin.userlesson.index', ['user_id' => $id]);
    }

    public function exams($id)
    {
        return redirect()->route('admin.quiz.index', ['user_id' => $id]);
    }

    public function __construct()
    {
        $request = Request::capture();
        $this->title = 'کاربر';
        $this->mainRoute = 'admin.user.index';
        $this->newRoute = 'admin.user.create';
        $this->saveRoute = 'admin.user.save';
        $this->colsNames = ["نام و نام خانوادگی", "نام کاربری", "موجودی کیف پول"];
        $this->cols = ["name", "username", "creditToNFT"];
        $this->actions = [
            ["route" => 'admin.user.edit', "icon" => 'fa fa-pencil', 'caption' => "ویرایش"],
            ["route" => 'admin.user.transactions', "icon" => 'fa fa-money', 'caption' => "تراکنش های کاربر"],
            ["route" => 'admin.user.lessons', "icon" => 'fa fa-book', 'caption' => "محصولات خریداری شده کاربر"],
            ["route" => 'admin.user.exams', "icon" => 'fa fa-address-card', 'caption' => "آزمون های شرکت کرده"],
            ["route" => 'admin.user.manual', "icon" => 'fa fa-user-plus', 'caption' => "مدیریت دسترسی محتوا"],
            ["route" => 'admin.user.delete', "icon" => 'fa fa-trash', 'caption' => "حذف کاربر", "type" => "ask"],
        ];
        $this->fields = [
            ["caption" => "نام و نام خانوادگی", "name" => "name", "valid" => "required"],
            ["caption" => "تلفن همراه ( نام کاربری)", "name" => "username", "valid" => ["required", new Mobile(), "unique:users,username," . $request->input('id')]],
            ["caption" => "موجودی کیف پول", "name" => "credit", "process" => "newTransaction", "valid" => "required"],
            ["caption" => "وضعیت حساب کاربری", "name" => "status_id", "valid" => "required|in:1,2", 'type' => 'select', "values" => ["1" => "فعال", "2" => "مسدود"]],
            ["caption" => "کد ملی", "name" => "melicode"],
            ["caption" => "آدرس", "name" => "address"],
            ["caption" => "نام پدر", "name" => "parent_name"],
            ["caption" => "کشور", "name" => "country"],
            ["caption" => "استان", "name" => "state"],
            ["caption" => "شهر", "name" => "city"],
            ["caption" => "کد پستی", "name" => "zipcode"],
            ["caption" => "روز تولد", "name" => "b_d"],
            ["caption" => "ماه تولد", "name" => "b_m"],
            ["caption" => "سال تولد", "name" => "b_y"],
            ["caption" => "عکس حساب کاربری", "name" => "pic","type"=>"image"],
        ];
        $this->searchKeys = [
            ["name" => "name", "title" => 'نام و نام خانوادگی', "type" => 'likeInput'],
            ["name" => "username", "title" => 'تلفن همراه ( نام کاربری)', "type" => 'likeInput'],
            ["name" => "credit", "title" => 'موجودی کیف پول', "type" => 'range'],
            ["name" => "status_id", "title" => 'وضعیت حساب کاربری', "type" => 'select', "value" => ["1" => "فعال", "2" => "مسدود"]],
        ];
        $this->GlobalRoutes = [
            ['route' => 'admin.user.export', 'title' => 'گرفتن خروجی اکسل'],
            ['route' => 'admin.user.import', 'title' => 'درون ریزی کاربران']
        ];
    }

    public function newTransaction($request, $value)
    {

        $user = User::findOrfail($request->input('id'));
        if ($user->credit == $value)
            return true;
        $trans = new Transaction();
        $trans->user_id = $user->id;
        $trans->type = "admin";
        $trans->rel_id = 0;
        $trans->res_id = "تغییر توسط ادمین";
        $trans->ref_id = "";
        $trans->status_id = '1';
        $trans->amount = $value - $user->credit;
        $trans->save();
        return $value;
    }

    public function manual($id)
    {
        $user = User::findOrfail($id);
        //get packages data
        $packages = Package::all();
        $userLessons = UserLesson::where('type', 'package')->where('user_id', $user->id)->get();
        $packagesData = [];
        foreach ($userLessons as $userPackage) {
            $packagesData[$userPackage->rel_id] = package::findOrFail($userPackage->rel_id)->title;
        }

        //get courses data
        $courses = Category::all();
        $userCourses = UserLesson::where('type', 'course')->where('user_id', $user->id)->get();
        $coursesData = [];
        foreach ($userCourses as $userCourse) {
            $coursesData[$userCourse->rel_id] = Category::findOrFail($userCourse->rel_id)->title;
        }

        //get lessons data
        $lessons = Lesson::all();
        $userLessons = UserLesson::where('type', 'lesson')->where('user_id', $user->id)->get();
        $lessonsData = [];
        foreach ($userLessons as $userLesson) {
            $lessonsData[$userLesson->rel_id] = Lesson::findOrFail($userLesson->rel_id)->title;
        }
        return view('admin.buy-manually.index', compact('user', 'packages', 'packagesData', 'courses', 'coursesData', 'lessons', 'lessonsData'));
    }

    public function manualSave(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $requestTypes = [
            'package' => $request->input('packages'),
            'course' => $request->input('courses'),
            'lesson' => $request->input('lessons')
        ];

        foreach ($requestTypes as $type => $requestType) {
            if (empty($requestType)) {
                UserLesson::where('user_id', $user->id)->where('type', $type)->delete();
            }
        }

        $types = array_filter($requestTypes, function ($value) {
            return !is_null($value) && $value !== '';
        });

        foreach ($types as $type => $selected) {
            $userLessons = UserLesson::where('user_id', $user->id)->where('type', $type)->get()->pluck('rel_id')->toArray();
            foreach ($selected as $id) {
                if (in_array($id, $userLessons)) {
                    unset($userLessons[array_search($id, $userLessons)]);
                } else {
                    unset($userLessons[array_search($id, $userLessons)]);
                    UserLesson::create([
                        'user_id' => $user->id,
                        'type' => $type,
                        'rel_id' => $id,
                        'transaction_id' => 0
                    ]);
                }
            }
            foreach ($userLessons as $contentId) {
                UserLesson::where('user_id', $user->id)->where('type', $type)->where('rel_id', $contentId)->delete();
            }
        }

        return redirect()->route('admin.user.manual', $user->id);
    }

    public function exportExcel()
    {
        $usersContents = UserLesson::all();
        return Excel::download(new UsersExport($usersContents), 'users.xlsx');
    }

    public function importIndex()
    {
        return view('admin.excel.index');
    }

    public function importExcel(Request $request)
    {
        $excelFile = $request->file('file');
        $headings = (new HeadingRowImport)->toArray($excelFile);
        Excel::import(new UsersImport($headings[0][0]), $excelFile);
        return redirect()->back();
    }
}
