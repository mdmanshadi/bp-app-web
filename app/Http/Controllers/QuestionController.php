<?php

namespace App\Http\Controllers;

use App\Category;
use App\Lesson;
use App\Question;
use App\QuestionAnswer;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    var $title = '';
    var $newRoute = '';
    var $colsNames = '';
    var $cols = '';
    var $actions = '';
    var $rows = '';
    var $saveRoute = '';
    var $mainRoute = '';
    var $fields = [];
    var $model = 'Question';

    public function __construct()
    {
        $request = Request::capture();
        $this->title = 'بانک سوال';
        $this->mainRoute = 'admin.question.index';
        $this->newRoute = 'admin.question.create';
        $this->saveRoute = 'admin.question.save';
        $this->reqData = ["lesson_id"=>$request->input('lesson_id')];
        if(!empty($request->input('lesson_id'))) {
            $this->mainRoute = ['admin.question.index', $request->input('lesson_id'), "lesson_id"];
            $this->newRoute = ['admin.question.create', $request->input('lesson_id'), "lesson_id"];
            $this->saveRoute = ['admin.question.save', $request->input('lesson_id'), "lesson_id"];
        }


        $this->colsNames = ["سوال","سطح"];
        $this->cols = ["title","levelText"];
        $this->actions = [
//            ["route"=>'admin.question.answer',"icon"=>'fa fa-question','caption'=>"پاسخ ها"],
            ["route"=>'admin.question.edit',"icon"=>'fa fa-pencil','caption'=>"ویرایش"],
            ["route"=>'admin.question.delete',"icon"=>'fa fa-trash','caption'=>"حذف","type"=>"ask"],

        ];
        $this->fields = [
            ["name"=>"lesson_id","type"=>"hidden","value"=>$request->input('lesson_id')],
            ["caption"=>"سوال","name"=>"title","valid"=>"required"],
            ["caption"=>"سطح","name"=>"level","type"=>"select","valid"=>"required|in:1,2,3","values"=>["1"=>"آسان","2"=>"متوسط","3"=>"سخت"]],
        ];

    }
    public function createQ(Request $request,$id)
    {
        $lesson = Lesson::findOrfail($id);

        return view('admin.question.create',compact('lesson'));
    }

    public function edit($id)
    {
        $question = Question::findOrfail($id);
        $lesson = Lesson::findOrfail($question->lesson_id);
        $answers = QuestionAnswer::where("question_id",$question->id)->orderBy("id","asc")->get();

        return view('admin.question.create',compact('lesson','question','answers'));
    }

    public function save(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'level' => 'required|in:1,2,3',
            'lesson_id' => 'required',
        ]);
        try {
            if (!empty($request->input('id'))) {
                $question = Question::findOrfail($request->input('id'));
            } else {
                $question = new Question();
            }


            $question->title = $request->input('title');
            $question->level = $request->input('level');
            $question->lesson_id = $request->input('lesson_id');
            $answers = $request->input('answer');

            if(count($answers) == 0) {
                throw new \Exception("لطفا حداقل یک سوال انتخاب کنید");
            }

            $question->save();

            QuestionAnswer::where("question_id", $question->id)->delete();
            if (!empty($answers) && is_array($answers)) {

                foreach ($answers as $i => $answer) {
                    if (empty($answer))
                        continue;
                    $thranswer = new QuestionAnswer();
                    $thranswer->value = $answer;
                    $thranswer->is_true = ($request->input('is_true')[$i] ?? 0) == "on" ? '1' : '2';
                    $thranswer->question_id = $question->id;
                    $thranswer->save();
                }
            }
            return redirect()->route('admin.question.index', ["lesson_id" => $request->input('lesson_id')])->with("success", "عملیات با موفقیت انجام شد.");
        }catch (\Exception $e){
            return redirect()->back()->with("danger",$e->getMessage());
        }
    }

    public function answer($id)
    {
        return redirect()->route('admin.answer.index',['question_id'=>$id]);
    }
}
