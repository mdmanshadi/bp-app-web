<?php

namespace App\Http\Controllers;

use App\Admin;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\Compound;
use Spatie\Permission\Models\Role;

class AdminController extends Controller
{
    var $title = '';
    var $newRoute = '';
    var $colsNames = '';
    var $cols = '';
    var $actions = '';
    var $rows = '';
    var $saveRoute = '';
    var $mainRoute = '';
    var $fields = [];
    var $model = 'Admin';

    public function __construct()
    {
        $request = Request::capture();
        $this->title = 'مدیر';
        $this->mainRoute = 'admin.admin.index';
        $this->newRoute = 'admin.admin.create';
        $this->saveRoute = 'admin.admin.save';
        $this->colsNames = ["نام", "نام کاربری"];
        $this->cols = ["name", "username"];
        $this->actions = [
            ["route" => 'admin.admin.edit', "icon" => 'fa fa-pencil', 'caption' => "ویرایش"],
            ["route" => 'admin.admin.delete', "icon" => 'fa fa-trash', 'caption' => "حذف مدیر", "type" => "ask"],
        ];
        $this->fields = [

        ];
        $this->searchKeys = [

        ];
        $this->GlobalRoutes = [
            ['route' => 'admin.roles.index', 'title' => 'مدیریت نقش ها'],
        ];
    }

    public function create(Request $request)
    {
        $roles = Role::all();
        return view('admin.admin.create', compact('roles'));
    }

    public function save(Request $request)
    {
        $id = $request->input('id');
        $name = $request->input('name');
        $username = $request->input('username');
        $password = $request->input('password');
        $role = $request->input('role');
        if (empty($id)) {
            $admin = Admin::create([
                'name' => $name,
                'username' => $username,
                'password' => bcrypt($password)
            ]);
        } else {
            $admin = Admin::findOrFail($id);
            $admin->name = $name;
            $admin->username = $username;
            $admin->password = bcrypt($password);
            $admin->save();
        }

        $admin->syncRoles($role);
        return redirect()->route('admin.admin.index');
    }

    public function edit($id)
    {
        $admin = Admin::findOrFail($id);
        $adminRole = $admin->roles()->first();
        $roles = Role::all();
        return view('admin.admin.edit', compact('id', 'admin', 'adminRole', 'roles'));
    }
}
