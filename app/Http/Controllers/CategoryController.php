<?php

namespace App\Http\Controllers;

use App\Category;
use App\Lesson;
use App\Package;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    var $title = '';
    var $newRoute = '';
    var $colsNames = '';
    var $cols = '';
    var $actions = '';
    var $rows = '';
    var $saveRoute = '';
    var $mainRoute = '';
    var $fields = [];
    var $model = 'Category';

    public function __construct()
    {
        $request = Request::capture();
        $this->title = 'دسته بندی';
        $this->mainRoute = 'admin.category.index';
        $this->newRoute = 'admin.category.create';
        $this->saveRoute = 'admin.category.save';
        $this->colsNames = ["عنوان","سرگروه","قیمت"];
        $this->cols = ["title","parentTitle","priceToNFT"];
        $this->actions = [
            ["route"=>'admin.category.edit',"icon"=>'fa fa-pencil','caption'=>"ویرایش"],
            ["route"=>'admin.category.delete',"icon"=>'fa fa-trash','caption'=>"حذف","type"=>"ask"],
            ["route"=>'admin.exam.create','params'=>["type"=>"category"],"icon"=>'fa fa-list','caption'=>"تنظیمات آزمون"],

            ];
        $this->fields = [
            ["caption"=>"عنوان","name"=>"title","valid"=>"required"],
            ["caption"=>"توضیحات","name"=>"description","type"=>"textarea","valid"=>"required"],
            ["caption"=>"تصویر","name"=>"pic","type"=>"image"],
            ["caption"=>"سرگروه","name"=>"parent_id","type"=>"select","values"=>Category::where("id","!=",$request->input('id'))->get()->pluck('title','id')->toArray()],
            ["caption"=>"قابل فروش ؟","name"=>"for_sale","type"=>"select","valid"=>"","values"=>["1"=>"فعال","2"=>"غیرفعال"]],
            ["caption"=>"قیمت فروش","name"=>"price","valid"=>"required|numeric"],
            ["caption"=>"تخفیف","name"=>"discount","valid"=>"required|numeric"],
            ["caption"=>"هدیه خرید (تومان)","name"=>"score","valid"=>"numeric"],
            ["caption"=>"وضعیت","name"=>"status_id","type"=>"select","valid"=>"","values"=>["1"=>"فعال","2"=>"غیرفعال"]],
        ];
        $this->searchKeys = [
            ["name"=>"title","title"=>'عنوان',"type"=>'likeInput'],
            ["name"=>"parent_id","title"=>'سرگروه',"type"=>'select',"value"=>Category::get()->pluck("title","id")->toArray()],
            ["name"=>"for_sale","title"=>'قابل فروش ؟',"type"=>'select',"value"=>["1"=>"فعال","2"=>"غیرفعال"]],
            ["name"=>"status_id","title"=>'وضعیت',"type"=>'select',"value"=>["1"=>"فعال","2"=>"غیرفعال"]],
            ["name"=>"price","title"=>'قیمت',"type"=>'range'],
        ];
    }

}
