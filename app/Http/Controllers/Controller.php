<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use File;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Storage;

class Controller extends BaseController
{

    var $searchKeys = [];
    var $GlobalRoutes = [];
    var $Excelcols = [];
    var $ExcelcolsNames = [];
    var $excelFormats = [];
    var $reqData = [];
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function index(Request $request)
    {

        $title = 'لیست '.$this->title;
        $newRoute = $this->newRoute;
        $colsNames = $this->colsNames;
        $cols = $this->cols;
        $actions = $this->actions;
        $GlobalRoutes = $this->GlobalRoutes;
        $rows = call_user_func_array("\App\\".$this->model . '::where', ["id","!=","0"]);
        if(!empty($this->spQuery))
            $rows = $this->spQuery;
        $searched = false;
        try {
            foreach ($request->all() as $req => $v) {
                $ignoreParms = ['_token','exportExcel','excelFormat'];
                if(in_array($req,$ignoreParms))
                    continue;
                if(empty($v))
                    continue;
                if(is_array($v)) {

                    if(!empty($v['min']) || !empty($v['max'])){
                        $min = $v['min'] ?? 0;
                        $max = $v['max'] ?? 0;

                        if($min > $max)
                            throw new \Exception("min-max error");

                        $rows = $rows->whereBetween($req, [$min, $max]);
                        $searched = true;

                    }if(!empty($v['min_date']) || !empty($v['max_date'])){
                        try {
                            $from = jdate()->fromFormat('Y/n/j H:i:s', $v['min_date'] . ' 00:00:00')->toCarbon()->toDateTimeString();
                        }catch (\Exception $e){
                            $from = null;
                        }
                        try {
                            $to = jdate()->fromFormat('Y/n/j H:i:s', $v['max_date'] . ' 23:59:59')->toCarbon()->toDateTimeString();
                        }catch (\Exception $e){
                            $to = null;

                        }
                        if(!empty($from) && !empty($to)){
                            $rows = $rows->whereBetween($req, [$from, $to]);
                        }elseif(!empty($from)){
                            $rows = $rows->where($req, ">=",$from);
                        }elseif(!empty($to)){
                            $rows = $rows->where($req, "=<",$from);
                        }

                        $searched = true;

                    }elseif(!empty($v['c']) && !empty($v['value'])){
                        if($v['c'] == 'LIKE') {
                            $rows = $rows->where($req, 'LIKE', '%'.$v['value'].'%');
                            $searched = true;
                        }
                    }
                }else {
                    $rows = $rows->where($req, $v);
                    $searched = true;
                }

            }

            $rows = $rows->orderBy("created_at","DESC")->get();
        }catch (\Exception $e){
            $rows = call_user_func_array("\App\\".$this->model . '::where', ["id","!=","0"]);
            $rows = $rows->get();
            session()->flash("danger","برخی از متد های جستجو دچار اشکال بودند");
        }
        $searchKeys = $this->searchKeys;


        $ids = [];
        foreach ($rows as $r){
            $ids[] = $r->id;
        }
        $exportExcel = $request->input('exportExcel');
        $excelFormats = $this->excelFormats;
        if($exportExcel == '1'){
            if(count($excelFormats) > 1){
                //chosen
                $chosenExcel = $excelFormats[$request->input('excelFormat')] ?? '';
                if(empty($chosenExcel))
                    return redirect()->back()->with("danger","لطفا نوع خروجی خود را انتخاب کنید");
                $cols = $this->Excelcols[$request->input('excelFormat')] ?? $this->cols;
                $colsNames = $this->ExcelcolsNames[$request->input('excelFormat')] ?? $this->colsNames;
            }else{
                $cols = (count($this->Excelcols) > 0 ? $this->Excelcols :  $this->cols);
                $colsNames = (count($this->ExcelcolsNames) > 0 ? $this->ExcelcolsNames :  $this->colsNames);
            }
            if(empty($cols) || empty($colsNames))
                return redirect()->back()->with("danger","پارامتر های خروجی  صحیح نیستند");


            return view('admin.print',compact('colsNames','rows','cols'));

        }else {
            return view('admin.list', compact('title', 'colsNames', 'cols', 'actions', 'rows', 'newRoute', 'searchKeys', 'searched', 'request', 'ids','excelFormats','GlobalRoutes'));
        }

        return view('admin.list',compact('title','colsNames','cols','actions','rows','newRoute','searchKeys','searched','request','GlobalRoutes'));
    }
    public function create(Request $request)
    {
        $title = $this->title.' جدید';
        $saveRoute = $this->saveRoute;
        $fields = $this->fields;
        $reqData = $this->reqData;
        return view('admin.new',compact('title','fields','saveRoute','request','reqData'));
    }
    public function edit($id)
    {
        try{
            $data = call_user_func_array("\App\\".$this->model . '::findOrFail', [$id]);

            $title = "ویرایش ".$this->title;
            $id = $data->id;
            $saveRoute = $this->saveRoute;
            $fields = $this->fields;

            return view('admin.new',compact('data','title','id','fields','saveRoute'));

        }catch (\Exception $e){
            return redirect()->back()->with("danger",trans('validation.error'));
        }
    }
    public function save(Request $request)
    {
        $model = "\App\\".$this->model;
        try {
            if (!empty($request->input('id')))
                $data = $data = call_user_func_array("\App\\".$this->model . '::findOrFail', [$request->input('id')]);

            else
                $data = new $model();

            $valid = [];
            foreach ($this->fields as $field) {
                if (!empty($field['valid'])) {
                    $valid[$field['name']] = $field['valid'];

                }
                if(!empty($field['process'])){
                    try{
                        $process = $field['process'];
                        $exp = $this->$process($request,$request->input($field['name']));
                    }catch (\Exception $e){
                        //
                        if(!empty($field['processForce'])) {
                            return redirect()->back()->with("danger",$e->getMessage());
                        }
                    }
                }
                if(!empty($field['skip']))
                    continue;

                $name = $field['name'];
                if (!empty($field['type']) && $field['type'] == 'hidden')
                    $data->$name = (empty($exp)) ? $field['value'] : $exp;
                elseif (!empty($field['type']) && $field['type'] == 'disable'){

                }elseif (!empty($field['type']) && $field['type'] == 'password'){
                    if(!empty($request->input($name))){
                        $data->$name = bcrypt((empty($exp)) ? $request->input($name) : $exp);
                    }

                }elseif (!empty($field['type']) && $field['type'] == 'location'){
                    $lt = empty($name) ? 'lat':$name.'_lat';
                    $lg = empty($name) ? 'lng':$name.'_lng';
                    $data->$lt = $request->input($lt);
                    $data->$lg = $request->input($lg);

                }elseif (!empty($field['type']) && $field['type'] == 'image'){
                    $image = $request->file($name);
                    if(!empty($image)) {
                        if (!empty($data->$name)) {
//                            File::delete(public_path($data->$name));
                            Storage::delete($data->$name);
                        }
                        $file_name = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME);
                        $saved_name = $file_name.substr(0,10).'-'.md5(time());
                        $path = Storage::putFile($this->model.'/'.$saved_name . '.' . $image->getClientOriginalExtension(), $image);
                        $data->$name = $path;

                    }
                }elseif (!empty($field['type']) && $field['type'] == 'file'){
                    $image = $request->file($name);
                    if(!empty($image)) {
                        if (!empty($data->$name)) {
                            Storage::delete($data->$name);
                        }
                        $file_name = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME);
                        $saved_name = $file_name.substr(0,10).'-'.md5(time());
                        $path = Storage::putFile($this->model.'/'.$saved_name . '.' . $image->getClientOriginalExtension(), $image);
                        $data->$name = $path;

                    }
                }elseif(is_array($request->input($name))){
                    $data->$name = json_encode($request->input($name));

                }else {

                    $data->$name = (empty($exp)) ? $request->input($name) : $exp;

                }
                $exp = null;
            }

        }catch (\Exception $e){
            if(config('app.debug'))
                dd($e);
            else
                return redirect()->back()->with("danger",trans('validation.error'));
        }

        $this->validate($request, $valid);

        $data->save();
        if(is_array($this->mainRoute)) {
            if(!empty($this->mainRoute[2])) {
                return redirect()->route($this->mainRoute[0], [$this->mainRoute[2] => $this->mainRoute[1]])->with("success", trans('validation.success'));

            }else {
                return redirect()->route($this->mainRoute[0], $this->mainRoute[1])->with("success", trans('validation.success'));
            }
        }else
            return redirect()->route($this->mainRoute)->with("success",trans('validation.success'));

    }
    public function delete(Request $request)
    {
        try {
            $id = $request->input('id');
            $data = call_user_func_array("\App\\".$this->model . '::findOrFail', [$id]);
            $data->delete();
            return redirect()->back()->with("success",trans('validation.success'));

        }catch (\Exception $e){
            return redirect()->back()->with("danger",trans('validation.error'));
        }
    }
}
