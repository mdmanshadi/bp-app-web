<?php

namespace App\Http\Controllers;

use App\Category;
use App\Lesson;
use App\Package;
use Illuminate\Http\Request;

class ExamController extends Controller
{
    var $title = '';
    var $newRoute = '';
    var $colsNames = '';
    var $cols = '';
    var $actions = '';
    var $rows = '';
    var $saveRoute = '';
    var $mainRoute = '';
    var $fields = [];
    var $model = 'Category';

    public function exam(Request $request,$id,$type)
    {
        //dd($type,$id);

        switch ($type){
            case 'category':
                $this->model = 'Category';
                $data = Category::findOrfail($id);
            break;
            case 'package':
                $this->model = 'Package';
                $data = Package::findOrfail($id);
            break;
            case 'lesson':
                $this->model = 'Lesson';
                $data = Lesson::findOrfail($id);
            break;
        }

        $this->title = 'آزمون';
        $this->mainRoute = 'admin.'.$type.'.index';
        $this->newRoute = 'admin.exam.create';
        $this->saveRoute = ['admin.exam.create',["id"=>$id,"type"=>$type]];

        if(empty($data))
            return redirect()->route($this->mainRoute)->with("danger",trans('validation.error'));

        $this->fields = [
            ["caption"=>"وضعیت آزمون","name"=>"status_id","type"=>"select","valid"=>"required|in:1,2","values"=>["1"=>"فعال","2"=>"غیرفعال"]],
            ["caption"=>"تعداد پاسخ ها","name"=>"answer_count","type"=>"text","valid"=>"required|numeric"],
            ["caption"=>"تعداد سوال سخت","name"=>"hard","type"=>"number","valid"=>"required|integer"],
            ["caption"=>"تعداد سوال متوسط","name"=>"normal","type"=>"number","valid"=>"required|integer"],
            ["caption"=>"تعداد سوال آسان","name"=>"easy","type"=>"number","valid"=>"required|integer"],
            ["caption"=>"حداکثر امتیاز","name"=>"max_score","type"=>"number","valid"=>"required|integer"],
            ["caption"=>"حداقل امتیاز قبولی","name"=>"min_score","type"=>"number","valid"=>"required|integer"],
            ["caption"=>"تعداد دفعات مجاز شرکت در آزمون","name"=>"max_use","type"=>"number","valid"=>"required|integer"],
            ["caption"=>"هزینه شرکت در آزمون اضافی","name"=>"extra_price","type"=>"number","valid"=>"required|integer"],
            ["caption"=>"زمان آزمون به دقیقه","name"=>"exam_time","type"=>"number","valid"=>"required|integer"],
            ["caption"=>"توضیحات آزمون","name"=>"description","type"=>"text","valid"=>"required"],


        ];
        if(!empty($request->input('_token'))){

            $this->validate($request,[
                'status_id'=>'required|in:1,2',
                'hard'=>'required|integer',
                'normal'=>'required|integer',
                'easy'=>'required|integer',
                'max_score'=>'required|integer',
                'min_score'=>'required|integer',
                'max_use'=>'required|integer',
                'extra_price'=>'required|integer',
                'exam_time'=>'required|integer',
                'description'=>'required',
            ]);
            $json = [
                "status_id"=>$request->input('status_id'),
                "answer_count"=>$request->input('answer_count'),
                "hard"=>$request->input('hard'),
                "normal"=>$request->input('normal'),
                "easy"=>$request->input('easy'),
                "max_score"=>$request->input('max_score'),
                "min_score"=>$request->input('min_score'),
                "max_use"=>$request->input('max_use'),
                "extra_price"=>$request->input('extra_price'),
                "exam_time"=>$request->input('exam_time'),
                "description"=>$request->input('description'),
            ];
            //dd(json_encode($json));
            $data->exam_detail = json_encode($json);
            $data->save();
            return redirect()->route($this->mainRoute)->with("data",trans('validation.success'));
        }


        $exam_detail = json_decode($data->exam_detail);
        $data->status_id = $exam_detail->status_id ?? '';
        $data->answer_count = $exam_detail->answer_count ?? '';
        $data->hard = $exam_detail->hard ?? '';
        $data->normal = $exam_detail->normal ?? '';
        $data->easy = $exam_detail->easy ?? '';
        $data->max_score = $exam_detail->max_score ?? '';
        $data->min_score = $exam_detail->min_score ?? '';
        $data->max_use = $exam_detail->max_use ?? '';
        $data->extra_price = $exam_detail->extra_price ?? '';
        $data->exam_time = $exam_detail->exam_time ?? '';
        $data->description = $exam_detail->description ?? '';

        $title = $this->title;
        $id = $data->id;
        $saveRoute = $this->saveRoute;
        $fields = $this->fields;

        return view('admin.new',compact('data','title','id','fields','saveRoute'));

    }
}
