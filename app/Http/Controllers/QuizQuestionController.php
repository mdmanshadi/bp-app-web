<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class QuizQuestionController extends Controller
{
    var $title = '';
    var $newRoute = '';
    var $colsNames = '';
    var $cols = '';
    var $actions = '';
    var $rows = '';
    var $saveRoute = '';
    var $mainRoute = '';
    var $fields = [];
    var $model = 'QuizQuestion';

    public function __construct()
    {
        $request = Request::capture();
        $this->title = 'نتیجه آزمون';
        $this->mainRoute = 'admin.quiz.index';
        $this->colsNames = ["سوال","پاسخ ها","پاسخ صحیح","پاسخ کاربر","وضعیت"];
        $this->cols = ["QuestionTitle","AnswersList","TrueAnswer","UserAnswer","AnswerStatus"];
        $this->actions = [

        ];
        $this->fields = [
            ["caption"=>"عنوان","name"=>"title","valid"=>"required"],
            ["caption"=>"تصویر","name"=>"pic","type"=>"image"],
            ["caption"=>"وضعیت","name"=>"status_id","type"=>"select","valid"=>"required|in:1,2","values"=>["1"=>"فعال","2"=>"غیرفعال"]],


        ];

    }
}
