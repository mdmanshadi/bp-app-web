<?php

namespace App\Http\Controllers;

use App\Category;
use App\Package;
use Illuminate\Http\Request;

class PackageController extends Controller
{
    var $title = '';
    var $newRoute = '';
    var $colsNames = '';
    var $cols = '';
    var $actions = '';
    var $rows = '';
    var $saveRoute = '';
    var $mainRoute = '';
    var $fields = [];
    var $model = 'Package';

    public function __construct()
    {
        $request = Request::capture();
        $this->title = 'پکیج';
        $this->mainRoute = 'admin.package.index';
        $this->newRoute = 'admin.package.create';
        $this->saveRoute = 'admin.package.save';
        $this->colsNames = ["عنوان","قیمت","وضعیت"];
        $this->cols = ["title","priceToNFT","statusText"];
        $this->actions = [
            ["route"=>'admin.package.edit',"icon"=>'fa fa-pencil','caption'=>"ویرایش"],
            ["route"=>'admin.package.delete',"icon"=>'fa fa-trash','caption'=>"حذف","type"=>"ask"],
            ["route"=>'admin.exam.create','params'=>["type"=>"package"],"icon"=>'fa fa-list','caption'=>"تنظیمات آزمون"],



        ];
        $this->fields = [
            ["caption"=>"عنوان","name"=>"title","valid"=>"required"],
            ["caption"=>"توضیحات","name"=>"description","type"=>"textarea","valid"=>"required"],
            ["caption"=>"تصویر","name"=>"pic","type"=>"image"],
            ["caption"=>"قیمت فروش","name"=>"price","valid"=>"required|numeric"],
            ["caption"=>"تخفیف","name"=>"discount","valid"=>"required|numeric"],
            ["caption"=>"هدیه خرید (تومان)","name"=>"score","valid"=>"numeric"],
            ["caption"=>"قابل فروش ؟","name"=>"for_sale","type"=>"select","valid"=>"","values"=>["1"=>"فعال","2"=>"غیرفعال"]],
            ["caption"=>"دوره های انتخابی","name"=>"categories","type"=>"multiselect","valid"=>"","values"=>Category::where("status_id","1")->where("for_sale","1")->get()->pluck("title","id")->toArray()],
            ["caption"=>"وضعیت","name"=>"status_id","type"=>"select","valid"=>"","values"=>["1"=>"فعال","2"=>"غیرفعال"]],


        ];
        $this->searchKeys = [
            ["name"=>"title","title"=>'عنوان',"type"=>'likeInput'],
            ["name"=>"status_id","title"=>'وضعیت',"type"=>'select',"value"=>["1"=>"فعال","2"=>"غیرفعال"]],
            ["name"=>"price","title"=>'قیمت',"type"=>'range'],
        ];
    }

}
