<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class AnswerController extends Controller
{
    var $title = '';
    var $newRoute = '';
    var $colsNames = '';
    var $cols = '';
    var $actions = '';
    var $rows = '';
    var $saveRoute = '';
    var $mainRoute = '';
    var $fields = [];
    var $model = 'QuestionAnswer';

    public function __construct()
    {
        $request = Request::capture();
        $this->title = 'پاسخ';
        $this->mainRoute = 'admin.answer.index';
        $this->newRoute = 'admin.answer.create';
        $this->saveRoute = 'admin.answer.save';

        $this->reqData = ["question_id"=>$request->input('question_id')];
        if(!empty($request->input('question_id'))) {
            $this->mainRoute = ['admin.answer.index', $request->input('question_id'), "question_id"];
            $this->newRoute = ['admin.answer.create', $request->input('question_id'), "question_id"];
            $this->saveRoute = ['admin.answer.save', $request->input('question_id'), "question_id"];
        }


        $this->colsNames = ["پاسخ","صحیح"];
        $this->cols = ["value","is_true"];
        $this->actions = [
            ["route"=>'admin.answer.edit',"icon"=>'fa fa-pencil','caption'=>"ویرایش"],
            ["route"=>'admin.answer.delete',"icon"=>'fa fa-trash','caption'=>"حذف","type"=>"ask"],

        ];
        $this->fields = [
            ["name"=>"question_id","type"=>"hidden","value"=>$request->input('question_id')],
            ["caption"=>"جواب","name"=>"value","valid"=>"required"],
            ["caption"=>"پاسخ صحیح ؟","name"=>"is_true","type"=>"select","valid"=>"required|in:1,2","values"=>["1"=>"بله","2"=>"خیر"]],
        ];

    }
}
