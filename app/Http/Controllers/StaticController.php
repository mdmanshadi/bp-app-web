<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StaticController extends Controller
{
    var $title = '';
    var $newRoute = '';
    var $colsNames = '';
    var $cols = '';
    var $actions = '';
    var $rows = '';
    var $saveRoute = '';
    var $mainRoute = '';
    var $fields = [];
    var $model = 'StaticModel';

    public function __construct()
    {
        $request = Request::capture();
        $this->title = 'صفحات';
        $this->mainRoute = 'admin.static.index';
        $this->newRoute = 'admin.static.create';
        $this->saveRoute = 'admin.static.save';
        $this->colsNames = ["عنوان"];
        $this->cols = ["title"];
        $this->actions = [
            ["route"=>'admin.static.edit',"icon"=>'fa fa-pencil','caption'=>"ویرایش"],
            ["route"=>'admin.static.delete',"icon"=>'fa fa-trash','caption'=>"حذف","type"=>"ask"],

            ];
        $this->fields = [
            ["caption"=>"عنوان","name"=>"title","valid"=>"required"],
            ["caption"=>"متن","name"=>"content","type"=>"textarea","valid"=>"required"],
            ["caption"=>"ایکون","name"=>"icon","type"=>"image"],
        ];
        $this->searchKeys = [
            ["name"=>"title","title"=>'عنوان',"type"=>'likeInput'],
        ];
    }
}
