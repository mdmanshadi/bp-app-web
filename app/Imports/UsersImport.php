<?php

namespace App\Imports;

use App\User;
use App\UserLesson;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class UsersImport implements ToCollection, WithHeadingRow
{
    protected $headings;

    public function __construct($headings)
    {
        $this->headings = $headings;
    }

    public function collection(Collection $rows)
    {
        $updateValues = [];
        $createValues = [];
        $contentValues = [];
        $excludedHeaders = ['lessons', 'courses', 'packages'];

        foreach ($rows as $row) {
            $user = User::find($row['id']);
            if ($user) {
                foreach ($this->headings as $heading) {
                    if ($user->$heading !== $row[$heading]) {
                        if (in_array($heading, $excludedHeaders)) {
                            $contentValues[$heading] = $row[$heading];
                            continue;
                        }
                        $updateValues[$heading] = $row[$heading];
                    }
                }
                User::update($updateValues);
            } else {
                foreach ($this->headings as $heading) {
                    if (in_array($heading, $excludedHeaders)) {
                        $contentValues[$heading] = $row[$heading];
                        continue;
                    }
                    $createValues[$heading] = $row[$heading];
                }
                $createdUser = User::create($createValues);

                $values = array_filter($contentValues, function ($value) {
                    return !is_null($value) && $value !== '';
                });

                foreach ($values as $type => $contentValue) {
                    $ids = explode(',',$contentValue);
                    foreach ($ids as $value) {
                        UserLesson::create([
                            'user_id' => $createdUser->id,
                            'type' => substr($type, 0, -1),
                            'rel_id' => $value,
                            'transaction_id' => 0
                        ]);
                    }
                }

            }
        }
    }
}
