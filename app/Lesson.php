<?php

namespace App;

class Lesson extends MainModel
{
    //
    protected $table = 'lessons';
    public function getCategoryTitleAttribute()
    {
        return $this->category->title ?? '-';
    }

    public function category()
    {
        return $this->belongsTo("App\Category","category_id");
    }


    public function getPicAttribute()
    {
        return empty($this->attributes['pic']) ? 'no-image-n.png' : $this->attributes['pic'];
    }

}
