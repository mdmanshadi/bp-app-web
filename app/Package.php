<?php

namespace App;

class Package extends MainModel
{
    protected $table = 'packages';

    public function getPicAttribute()
    {
        return empty($this->attributes['pic']) ? 'no-image-n.png' : $this->attributes['pic'];
    }

    public function courses()
    {
        return $this->hasMany(Category::class,'parent_id','id');
    }

    public function lessons()
    {
        return $this->hasManyThrough(Lesson::class,Category::class,'parent_id','category_id','id','id');
    }
}
