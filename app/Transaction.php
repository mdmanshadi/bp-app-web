<?php

namespace App;

class Transaction extends MainModel
{
    //
    protected $table = 'transactions';

    public function user()
    {
        return $this->belongsTo("App\User","user_id");
    }

    public function getuserNameAttribute()
    {
        return $this->user->name ?? '-';
    }

    //دریافت نوع خرید
    public function getTypeTextAttribute()
    {
        switch ($this->type){
            case 'course':
                return "خرید دوره";
                break;
            case 'package':
                return "خرید پکیج";
                break;
            case 'lesson':
                return "خرید درس";
                break;
            case 'quiz':
                return "خرید آزمون";
                break;
            case 'admin':
                return "تغییر موجودی";
                break;
            case 'wallet':
                return "شارژ کیف پول ";
            break;
        }
    }
    public function getStatusTextAttribute()
    {
        return $this->status_id == '1' ? "موفق" : "ناموفق";
    }
    public function getFaCreatedAtAttribute()
    {
        return jdate($this->created_at)->format("Y/m/d H:i:s");
    }

}
