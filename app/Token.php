<?php

namespace App;


class Token extends MainModel
{
    protected $table = 'tokens';

    public static function FindUser($token)
    {
        //پیداکردن کاربر با استفاده از توکن موبایل
        try {
            $token = Token::where("user_token",$token);
            if ($token->count() == 0)
                throw new \Exception("user Not Found");
            $token = $token->get()->first();
            $user = User::findOrfail($token->rel_id);
            if($user->status_id != '1')
                throw new \Exception("userDeactived");
            return $user;

        }catch (\Exception $e){
            return false;
        }
    }
}
