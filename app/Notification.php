<?php

namespace App;

class Notification extends MainModel
{
    //
    protected $table = 'notifications';
    public function getFaCreatedAtAttribute()
    {
        return jdate($this->created_at)->format("Y/m/d H:i:s");
    }

}
