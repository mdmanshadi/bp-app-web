<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MainModel extends Model
{
    public function getStatusTextAttribute()
    {
        return $this->status_id == '1' ? "فعال" : "غیرفعال";
    }
}
