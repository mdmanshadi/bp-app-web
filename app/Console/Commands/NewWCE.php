<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use File;
class NewWCE extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'wce:create {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make WCE';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $name = strtolower($this->argument('name'));
        $title = $this->anticipate('What is Title ?',[$name]);
        $model = $this->anticipate('What is Model Name ?',[$name]);
        $table = $this->anticipate('What is Table Name ?',[$model]);
        $rName = $name;
        $name = ucfirst($name);


        $content = file_get_contents(base_path("app/console/Commands/WCE/NewController.php.new"));
        $content = str_replace("__NAME__",$name,$content);
        $content = str_replace("__TITLE__",$title,$content);
        $content = str_replace("__RNAME__",strtolower($name),$content);
        $content = str_replace("__MODEL__",ucfirst($model),$content);

        $set = false;
        if(File::exists(base_path('app/Http/Controllers/'.$name.'Controller.php'))){
            if ($this->confirm('Controller Exists . Do you wish to continue?')) {
                $set = true;
            }
        }else $set = true;
        if(!empty($set))
            File::put(base_path('app/Http/Controllers/'.$name.'Controller.php'),$content);

        $content = file_get_contents(base_path("app/console/Commands/WCE/NewModel.php.new"));
        $content = str_replace("__NAME__",ucfirst($model),$content);
        $content = str_replace("__TABLE__",$table,$content);

        $set = false;
        if(File::exists(base_path('app/'.ucfirst($name).'.php'))){
            if ($this->confirm('Model Exists . Do you wish to continue?')) {
                $set = true;
            }
        }else $set = true;
        if(!empty($set))
            File::put(base_path('app/'.ucfirst($model).".php"),$content);

        //c/e/d/l/s
        //l/c/e/s/d
        $route = $this->anticipate("Enter Routes Format (seprate by /) :",["l/c/e/s/d"]);
        $route = explode("/",$route);





        $d = '';
        $name .= "Controller";
        foreach ($route as $r) {
            if($r == 'l')
                $d.="Route::any('/".$rName."', '".$name."@index')->name('admin.".$rName.".index');\n";
            elseif($r == 'c')
                $d.="Route::get('/".$rName."/create', '".$name."@create')->name('admin.".$rName.".create');\n";
            elseif($r == 'e')
                $d.="Route::get('/".$rName."/{id}/edit', '".$name."@edit')->name('admin.".$rName.".edit');\n";
            elseif($r == 's')
                $d.="Route::post('/".$rName."/save', '".$name."@save')->name('admin.".$rName.".save');\n";
            elseif($r == 'd')
                $d.="Route::post('/".$rName."/delete', '".$name."@delete')->name('admin.".$rName.".delete');\n";

        }
        $d .= "\n\n\n";

        $routes = file_get_contents(base_path('routes/web.php'));
        $route = $d;

        if (strpos($routes, $route) !== false) {
            $this->info("routes is already configured !");

        }else{
            file_put_contents(
                base_path('routes/web.php'),
                $route,
                FILE_APPEND
            );
            $this->info("routes configured !");

        }

        $this->info("WCE created successfully !");
        //$this->info("please run 'php artisan migrate'");
    }
}
