@extends('admin.layout')
@section('content')
    @if(!empty($searchKeys))
        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-heading clearfix">
                    <h4 class="panel-title">گزارش گیری و جستجو</h4>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" method="post" action="">
                        {{csrf_field()}}
                        @foreach($searchKeys as $searchKey)
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="{{$searchKey['name'] ?? ''}}" class="col-md-3 control-label">{{$searchKey['title'] ?? $searchKey['name']}}</label>
                                    <div class="col-md-9">
                                        <?php $type = $searchKey['type']; ?>

                                    @switch($searchKey['type'] ?? 'text')
                                            @case($type == 'select')
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <select class="form-control" name="{{$searchKey['name'] ?? ''}}">
                                                            <option value="">انتخاب کنید</option>
                                                        @foreach($searchKey['value'] ?? [] as $id => $vals)

                                                                <option @if($request->input($searchKey['name']) == $id) selected @endif value="{{$id}}">{{$vals}}</option>
                                                        @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            @break
                                            @case($type == 'range')
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control" name="{{$searchKey['name'] ?? ''}}[min]" placeholder="{{$searchKey['title'] ?? $searchKey['name']}} از" value="{{$request->input($searchKey['name'])['min'] ?? ''}}">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control" name="{{$searchKey['name'] ?? ''}}[max]" placeholder="{{$searchKey['title'] ?? $searchKey['name']}} تا" value="{{$request->input($searchKey['name'])['max'] ?? ''}}">
                                                    </div>
                                                </div>
                                            @break
                                            @case($type == 'rangedate')
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <input type="text" class="form-control" name="{{$searchKey['name'] ?? ''}}[min_date]" placeholder=" از (مثال : 1397/12/10)" value="{{$request->input($searchKey['name'])['min_date'] ?? ''}}">
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" class="form-control" name="{{$searchKey['name'] ?? ''}}[max_date]" placeholder=" تا (مثال : 1398/12/10)" value="{{$request->input($searchKey['name'])['max_date'] ?? ''}}">
                                                </div>
                                            </div>

                                            @break
                                            @case($type == 'input')
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <input type="text" class="form-control" name="{{$searchKey['name'] ?? ''}}" placeholder="جستجوی دقیق {{$searchKey['title']}}" value="{{$request->input($searchKey['name']) ?? ''}}">
                                                </div>
                                            </div>

                                            @break
                                            @case($type == 'likeInput')
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <input type="hidden" name="{{$searchKey['name'] ?? ''}}[c]" value="LIKE">
                                                    <input type="text" class="form-control" name="{{$searchKey['name'] ?? ''}}[value]" placeholder="جستجوی بخشی {{$searchKey['title']}}" value="{{$request->input($searchKey['name'])['value'] ?? ''}}">
                                                </div>
                                            </div>

                                            @break

                                                @default
                                                <input type="text" class="form-control" name="{{$searchKey['name'] ?? ''}}" placeholder="{{$searchKey['title'] ?? $searchKey['name']}}" value="{{$request->input($searchKey['name']) ?? ''}}">
                                            @break

                                        @endswitch
                                        <p class="help-block help-block-p-text">{{$searchKey['caption'] ?? ''}}</p>

                                    </div>
                                </div>
                            </div>
                        @endforeach

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-success horizontal-form-button">جستجو</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endif
    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">{{$title}} @if(!empty($searched)) -  (نتیجه جستجو شده) <a href="{{url()->current()}}" style="color: #799e05">نمایش همه نتایج</a>@endif</h4>
            </div>
            <div class="panel-body">
                @if(!empty($newRoute))
                    <a class="btn btn-success m-b-sm" href="{{route($newRoute)}}">افزودن رکورد جدید</a>
                @endif
                <div class="table-responsive">
                    <table id="table" class="display table table-data-width">
                        <thead>
                        <tr>
                            <th>ردیف</th>
                        @foreach($colsNames as $col)
                            <th>{{$col}}</th>
                        @endforeach
                            @if(!empty($actions))
                            <th>عملیات</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i=1; ?>
                        @foreach($rows as $row)
                        <tr>
                            <td>{{$i++}}</td>
                            @foreach($cols as $col)
                                @if(strpos($col,"ToNFT") !== false)
                                    <?php
                                    $col = explode("ToNFT",$col)[0] ?? 'n';
                                    ?>
                                    <td>{!! (number_format($row->$col ?? '0')." تومان")  !!}</td>
                                @elseif(strpos($col,"ToMetr") !== false)
                                    <?php
                                    $col = explode("ToMetr",$col)[0] ?? 'n';
                                    ?>
                                    <td>{!! (number_format($row->$col ?? '0')." متر")  !!}</td>
                                @elseif(strpos($col,"ToPC") !== false)
                                    <?php
                                    $col = explode("ToPC",$col)[0] ?? 'n';
                                    ?>
                                    <td>{!! (number_format($row->$col ?? '0')." درصد")  !!}</td>
                                @else
                                    <td>{!! $row->$col !!}</td>
                                @endif
                            @endforeach
                            @if(!empty($actions))
                                <td>
                                    @foreach($actions as $action)
                                        <a data-toggle="tooltip" data-placement="top" title="{{$action['caption'] ?? ''}}" style="margin-left: 10px" href="{{route($action['route'] ?? 'home',(!empty($action['params']) ? ["id"=>$row->{$action['params']}] : ["id"=>$row->id]))}}"><i class="{{$action['icon'] ?? ''}}"></i> </a>
                                    @endforeach
                                </td>
                            @endif


                        </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $("#table").dataTable();
    </script>
@endsection
