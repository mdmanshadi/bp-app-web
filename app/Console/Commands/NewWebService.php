<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use File;
class NewWebService extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'wbs:create {name} {version}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make webservice';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        if(!File::exists(base_path('app/ApiModule/MainApi.php')))
            return $this->error("please first run 'make:webservice' command");
        $name = ucfirst($this->argument('name'))."Api";
        $version = $this->argument('version') ?? 1;
        $version = "V".$version;
        $this->line($name.'/'.$version);

        $content = file_get_contents(base_path("app/console/Commands/Base/TestApi.php.new"));
        $content = str_replace("__THEVERSION__",$version,$content);
        $content = str_replace("__THETRAIT__",$name."Module",$content);
        $content = str_replace("__THENAME__",$name,$content);

        File::makeDirectory(base_path('app/ApiModule/'.$version) , 0777, true, true);
        File::put(base_path('app/ApiModule/'.$version.'/'.$name.'.php'),$content);


        File::makeDirectory(base_path('app/ApiModule/Module') , 0777, true, true);
        $content = file_get_contents(base_path("app/console/Commands/Base/TestApiModule.php.new"));
        $content = str_replace("__NAME__",$name."Module",$content);
        File::put(base_path('app/ApiModule/Module/'.$name."Module.php"),$content);

        $this->info("webservice  created successfully !");
        $this->info("please run 'php artisan migrate'");
    }
}
