<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use File;
class MakeWebService extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:webservice';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make webservice Scaffold';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        File::copy(base_path('app/console/Commands/Base/ServiceController.php.new'),base_path('app/Http/Controllers/ServiceController.php'));
        $this->info("ServiceController.php created successfully !");

        File::copy(base_path('app/console/Commands/Base/Token.php.new'),base_path('app/Token.php'));
        $this->info("Token.php created successfully !");

        File::copy(base_path('app/console/Commands/Base/create_token_table.php.new'),base_path('database/migrations/create_token_table.php'));
        $this->info("create_token_table.php created successfully !");

        File::makeDirectory(base_path('app/ApiModule') , 0777, true, true);
        $this->info("ApiModule Directory created successfully !");

        File::copy(base_path('app/console/Commands/Base/MainApi.php.new'),base_path('app/ApiModule/MainApi.php'));
        $this->info("MainApi.php created successfully !");

        $routes = file_get_contents(base_path('routes/api.php'));
        $route = "Route::any('{version}/{category}/{function}', 'ServiceController@parser')->name('service.parser');";

        if (strpos($routes, $route) !== false) {
            $this->info("routes is already configured !");

        }else{
            file_put_contents(
                base_path('routes/api.php'),
                $route,
                FILE_APPEND
            );
            $this->info("routes configured !");

        }


        $this->info("webservice Scaffold created successfully !");
        $this->info("please run 'php artisan migrate'");
    }
}
