<?php

namespace App;

class QuizQuestion extends MainModel
{
    //
    protected $table = 'quiz_questions';

    public function question()
    {
        return $this->belongsTo("App\Question","question_id");
    }

    public function user()
    {
        return $this->belongsTo("App\User","user_id");
    }

    public function quiz()
    {
        return $this->belongsTo("App\QUiz","quiz_id");
    }

    public function getQuestionTitleAttribute()
    {
        return $this->question->title;
    }

    public function answers()
    {
        return $this->hasMany("App\QuizQuestionAnswer","question_id");
    }

    public function getAnswersListAttribute()
    {
        $out = "";

        foreach ($this->answers as $answer) {
         $out .= $answer->title.'<br>';
        }
        return $out;
    }

    public function TrueAnswer()
    {
        foreach ($this->answers as $answer) {
            if(($answer->answer->is_true ?? 0) == '1')
                return $answer;
        }
        return "-";
    }

    public function UserAnswer()
    {
        foreach ($this->answers as $answer) {
            if(($answer->user_select ?? 0) == '1')
                return $answer;
        }
        return null;
    }

    public function getTrueAnswerAttribute()
    {
        return $this->TrueAnswer()->title ?? '-';
    }

    public function getUserAnswerAttribute()
    {
        return $this->UserAnswer()->title ?? '-';
    }


    public function getAnswerStatusAttribute()
    {
        return ($this->UserAnswer()->id ?? 0) == ($this->TrueAnswer()->id ?? -1) ? ' صحیح' : ' غلط';
    }

}
