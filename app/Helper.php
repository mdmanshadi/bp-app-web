<?php

namespace App;

use Morilog\Jalali\Jalalian;

class Helper
{
    public static function isPurchased($rel,$rel_id,$user_id,$recursive = true)
    {
        //بررسی اینکه آیا محصول خریداری شده یا نه
        $check = UserLesson::where("user_id",$user_id)->where("type",$rel)->where("rel_id",$rel_id);
        if($check->count() > 0)
            return true;
        if(!$recursive)
            return false;
        if($rel == 'course'){
            //بررسی در پکیج های خریداری شده که آیا این دوره در پکیج خریداری شده و یا تکی خریداری شده
            //check packages
            $course = Category::find($rel_id);
            $pkgs = Package::where('categories', 'like', '%"'.$course->id.'"%')->get()->pluck("id")->toArray();
            $check = UserLesson::where("user_id",$user_id)->where("type","package")->whereIn("rel_id",$pkgs);
            if($check->count() > 0)
                return true;
        }
        if($rel == 'lesson'){
            //بررسی در پکیج و دوره ها و درس های خریداری شده که آیا این درس در این خرید ها موجود هست یا نه
            //search in courses & packages
            $lesson = Lesson::find($rel_id);
            $check = UserLesson::where("user_id",$user_id)->where("type","course")->where("rel_id",$lesson->category_id);
            if($check->count() > 0)
                return true;
            //check packages
            $course = Category::find($lesson->category_id);
            $pkgs = Package::where('categories', 'like', '%"'.$course->id.'"%')->get()->pluck("id")->toArray();
            $check = UserLesson::where("user_id",$user_id)->where("type","package")->whereIn("rel_id",$pkgs);
            if($check->count() > 0)
                return true;
            //کنترل دوره های بالاتر برای پیدا کردن خرید
            if(!empty($course->parent_id)){
                return Helper::isPurchased('course',$course->parent_id,$user_id,$recursive);
            }
        }
        return false;
    }

    public static function getAllLessons($id,$type)
    {
        //دریافت همه دروس یک دوره یا پکیج
        $lessons = [];
        if($type == 'course'){
            $courses = Category::where("parent_id",$id)->get();
            $lessons = array_merge($lessons,Lesson::where("category_id",$id)->get()->pluck('id')->toArray());

            foreach ($courses as $course) {
                $lessons = array_merge($lessons,Helper::getAllLessons($course->id,"course"));
            }
        }elseif($type == 'package'){
            $pkg = Package::findOrfail($id);
            $cats = json_decode($pkg->categories);
            foreach ($cats as $cat) {
                $lessons = array_merge($lessons,Helper::getAllLessons($cat,"course"));

            }
        }

        return $lessons;
    }
    public static function validateMobile($mobile)
    {
        //بررسی درستی موبایل که با ۰۹ شروع شده باشد
        $mob = str_split($mobile);
        $mob0 = $mob[0] ?? '-1';
        $mob1 = $mob[1] ?? '-1';
        if($mob0 == '0' && $mob1 == '9'){
            if(strlen($mobile) == 11)
                return true;
        }
        return false;
    }
    public static function sendSms($user_id,$text,$direct = true,$type = 2,$template = 'verification')
    {
        //ارسال پیامک وب سرویس رادپرداز
        try{
            //اگر direct -> true مقدار user_id برابر شماره موبایل خواهد بود
            if(!$direct) {
                $user = User::findOrfail($user_id);
                $mobile = $user->username ?? '0';
            }else{
                $mobile = $user_id;
            }
            if(!Helper::ValidateMobile($mobile))
                return false;
            if($type == 1) {
                //ارسال پیامک عادی
                $url = "https://api.kavenegar.com/v1/454D3637707A6A4B43705670326B3232434C3930713147594D424A7132705851/sms/send.json";
                $param = array
                (
                    'receptor' => $mobile,
                    'message' => $text,
                    'sender' => '10004346',
                );
            }elseif($type == 2){
                //ارسال پیامک اعتبار سنجی
                $url = "https://api.kavenegar.com/v1/454D3637707A6A4B43705670326B3232434C3930713147594D424A7132705851/verify/lookup.json";
                $param = array
                (
                    'receptor' => $mobile,
                    'token' => $text,
                    'template' => $template,
                );
            }

            $handler = curl_init($url);
            curl_setopt($handler, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($handler, CURLOPT_POSTFIELDS, $param);
            curl_setopt($handler, CURLOPT_RETURNTRANSFER, true);
            $response2 = curl_exec($handler);
            $response2 = json_decode($response2);
            //dump($response2);

            //بازگشت پاسخ
            return $response2;

        }catch (\Exception $e){
            return false;
        }

    }

    public static function sendNotificationToAndroid($message,$data,$token,$driver = false)
    {
        //ارسال نوتیفیکیشن به اندروید
        try {
            $url = 'https://fcm.googleapis.com/fcm/send';

            $fields = array(
                'registration_ids' => [$token],
                'data' => array(
                    "message" => $message,
                    'data' => $data,
                )
            ,'priority'=>10
            );
            $fields = json_encode($fields);

            if($driver)
                $headers = array(
                    'Authorization: key=' . "AAAAJwxceQg:APA91bHVgQBvdYghHdkgHf_K2yIR0yfd7f8V_u4DFru9dhH3VIE7G0FZRnzTEBv-_K2aaMhv5rBdR1ClntHuA8Ei_ow6o9xjI4lsbDJK_t3xbWWs2lgOVaZhtkwZ058IItbM2D5wYLk1",
                    'Content-Type: application/json'
                );
            else
                $headers = array(
                    'Authorization: key=' . "AAAAwqkTiyE:APA91bECSDbpPNmT0mqvSyCJD6ocuiOqPGX1NKyW0fbpBzNIzMaW4AIh16cFpOZE5Vb6OcBLKFBU23klPcpd-Eqv-XXazk-T7yDX0aCxxY_15bu7XLGQt-LLDyFukt9Fjb6MCEXaIt1Z",
                    'Content-Type: application/json'
                );

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

            $result = curl_exec($ch);

            //dd($result);
            curl_close($ch);
            return true;
        }catch (\Exception $e){
            return false;
        }
    }

    public static function sendNotificationToIOS($message,$data,$token,$driver = false)
    {
        //ارسال نوتیفیکیشن به ای او اس
        try {
            $deviceToken = $token;
            $passphrase = '204059';
            if($driver)
                $pemfilename = public_path('certs/eltaxidriver.pem');
            else
                $pemfilename = public_path('certs/eltaxi.pem');

            $body['aps'] = array(
                'alert' => array(
                    'title' => $message,
                    'body' => $message,
                    'extra' => $data,
                ),
                'badge' => 0,
                'sound' => 'default',
            );
            $ctx = stream_context_create();
            stream_context_set_option($ctx, 'ssl', 'local_cert', $pemfilename);
            stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
            $fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx); // Open a connection to the APNS server
            if (!$fp)
                return false;
            $payload = json_encode($body); // Encode the payload as JSON
            $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload. chr(5) . pack('n', 1) . chr(10);; // Build the binary notification
            $result = fwrite($fp, $msg, strlen($msg)); // Send it to the server
            fclose($fp); // Close the connection to the server
            if (!$result)
                return false;
            else
                return true;
        }catch (\Exception $e){
            return;
        }
    }

    public static function toDateTime($value)
    {
        //تبدیل تاریخ شمسی به کربن
        return Jalalian::fromFormat('Y/n/j', $value)->toCarbon();

    }
}
