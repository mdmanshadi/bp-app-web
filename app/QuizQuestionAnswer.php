<?php

namespace App;

class QuizQuestionAnswer extends MainModel
{
    //
    protected $table = 'quiz_question_answers';

    public function answer()
    {
        return $this->belongsTo("App\QuestionAnswer","answer_id");
    }

    public function getTitleAttribute()
    {
        return $this->answer->value ?? '-';
    }

}
