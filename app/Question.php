<?php

namespace App;

class Question extends MainModel
{
    //
    protected $table = 'questions';

    public function getlevelTextAttribute()
    {
        if($this->level == '1'){
            return "آسان";
        }elseif($this->level == '2'){
            return "متوسط";
        }else{
            return "سخت";

        }
    }

}
