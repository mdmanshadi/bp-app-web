<?php

namespace App\Exports;

use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class UsersExport implements FromCollection, WithMapping, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */
    protected $usersContents;
    public function collection()
    {
        return User::all();
    }

    public function __construct($usersContents)
    {
        $this->usersContents = $usersContents;
    }

    public function headings(): array
    {
        return [
            'id',
            'name',
            'username or phone',
            'state',
            'city',
            'email',
            'credit',
            'lessons',
            'courses',
            'packages'
        ];
    }

    public function map($user): array
    {
        return [
            $user->id,
            $user->name,
            $user->username,
            $user->state,
            $user->city,
            $user->email,
            $user->credit,
            $this->usersContents->where('user_id',$user->id)->where('type','lesson')->values()->map(function ($item){
                return $item->rel_id;
            })->join(','),
            $this->usersContents->where('user_id',$user->id)->where('type','course')->values()->map(function ($item){
                return $item->rel_id;
            })->join(','),
            $this->usersContents->where('user_id',$user->id)->where('type','package')->values()->map(function ($item){
                return $item->rel_id;
            })->join(',')
        ];
    }


}
