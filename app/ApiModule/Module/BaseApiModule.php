<?php
/**
 *  Copyright (c) 2019.
 *  Developer : Mehdi Meshkatian :)
 */

namespace App\ApiModule\Module;

use App\Category;
use App\Coupon;
use App\Download;
use App\FileModel;
use App\Helper;
use App\Lesson;
use App\Package;
use App\Question;
use App\QuestionAnswer;
use App\Quiz;
use App\QuizQuestion;
use App\QuizQuestionAnswer;
use App\Rate;
use App\Slider;
use App\SmsIR_VerificationCode;
use App\StaticModel;
use App\Token;
use App\Transaction;
use App\User;
use App\UserLesson;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Ipecompany\Smsirlaravel\Smsirlaravel;
use Validator;
use Str;
use App\Setting;
use App\Fav;

trait BaseApiModule
{
    private function validateReq($request, $inputs)
    {
        //از این متد برای اعتبار سنجی درخواست های ارسالی استفاده میشه
        $valid = Validator::make($request->all(), $inputs);
        if ($valid->fails()) {
            //در صورت وجود خطا دریافت اولین خطا
            $error = array_values(json_decode(json_encode($valid->messages()), true));
            $error = $error[0][0] ?? "لطفا همه موارد درخواستی را تکمیل کنید";
            throw new \Exception($error);
        }
        return true;
    }

    public function login($request)
    {
        //تابع ورود به سیستم
        $username = $request->input('username');
        $code = $request->input('code');
        $user = User::where("username", $username);
        $template = 'verification';
        if (config('auth.APP') == 'Nejateman')
            $template = 'nejatverification';
        if ($user->count() == 0) {
            //اگر یوزر وجود نداره باید ثبت نام صورت بگیره و تلفن همراه اعتبار سنجی بشه
            if (Helper::validateMobile($username)) {

                //ساخت یوزر جدید در دیتابیس
                $user = new User();
                $user->name = '';
                $user->username = $username;
                $user->password = bcrypt($username . rand(1, 1000));
                $user->credit = 0;
                $user->status_id = 1;
                $user->ref_code = $request->input('ref_code');
                //تولید کد فعال سازی یکتا به صورت رندوم
                $user->sms_verify_code = rand(1000, 9999);
                $user->save();

                //ارسال اس ام اس فعال سازی
                Smsirlaravel::sendVerification($user->sms_verify_code,$user->username);
                // اگر مقدار new برای اپ ارسال بشه اپ متوجه میشه که فرآیند برای ثبت نام هست و از کاربر درخواست نام و نام خانوادگی و اطلاعات ثبت نام را میکنه
                return "new";

            } else throw new \Exception("تلفن همراه وارد شده صحیح نیست");

        } else $user = $user->get()->first();
        //در صورت وجود داشتن کاربر عملیات ورود به سیستم انجام خواهد شد
        //بررسی این که کاربر مسدود نباشد
        if ($user->status_id != '1')
            throw new \Exception("حساب کاربری شما مسدود شده است");
        if (empty($code)) {
            //اگر هنوز کاربر کد را ارسال نکرده باشد // یعنی ثبت نام در مرحله وارد کردن شماره موبایل باشد کد فعال سازی ساخته شده و ارسال میشه
            $user->sms_verify_code = rand(1000, 9999);
            $user->save();
            Smsirlaravel::sendVerification($user->sms_verify_code,$user->username);
            //اگر به هر دلیلی نام کاربر خالی بود (چه تازه ثبت نام کرده و یا اگر نام خودشو خالی گذاشته با کاربر مشابه کاربر جدید برخورد میشه و درخواست میشه اطلاعاتش رو کامل وارد کنه
            if (empty($user->name))
                return "new";
            else return "1";

        } else {
            //درصورتی که عملیات در مرحله دوم یعنی ورود کد باشه کد بررسی شده و در صورت صحیح بودن وارد سامانه میشه
            if ($user->sms_verify_code != $code)
                throw new \Exception("کد وارد شده صحیح نیست");
            if (empty($user->name)) {
                //اگر نام کاربر خالی بود حتما بررسی میکنه که نام رو وارد کرده باشه ( جهت جلوگیری از ثبت نام بدون نام)
                $this->validateReq($request, [
                    'name' => 'required',
                ]);
                $user->name = $request->input('name');
                $user->save();
            }

            //بررسی اینکه در جدول توکن ها این یوزر قبلا وجود داشته یا نه
            $token = Token::where("rel", "user")->where("rel_id", $user->id);
            if ($token->count() > 0)
                $token = $token->get()->first();
            else
                $token = new Token();
            // در صورت وجود نداشتن ساخت یک توکن جدید

            $token->rel = "user";
            $token->rel_id = $user->id;
            //ساخت یک رشته کد رندوم ۱۲۰ کاراکتری برای توکن کاربران
            $token->user_token = Str::random(120);
            $token->save();
            return $token->user_token;


        }

    }
    public function getConfig()
    {
        return Setting::select("title","value")->get()->pluck("value","title")->toArray();
    }
    public function GetMain($request)
    {

        //تابع دریافت اطلاعات اصلی
        //اگر توکن کاربر وجود نداشته باشه خطا ارسال میکنه که در اپ صفحه لاگین رو میاره
        if (empty(config('auth.THIS_USER')))
            throw new \Exception("Not Logged In");
        $user = User::findOrfail(config('auth.THIS_USER'));
        $return = [];
        //////////////////////////////////
        //برگرداندن اطلاعات کلی کاربر مثل اعتبار و تصویر و نام و ...
        $return['user'] = [
            "id" => $user->id,
            "name" => $user->name,
            "credit" => $user->credit,
            "pic" => Storage::url($user->pic ?? 'man.png'),
            "credit_text" => number_format($user->credit) . " تومان"
        ];
        //////////////////////////////////
        $products = [];
        //////////////
        /// جدیدترین دوره ها
        $cats = Category::where("for_sale", "1")->where("status_id", "1")->select("id", "title", "pic", "price", "discount", "for_sale", "score")->inRandomOrder()->get();
        foreach ($cats as $cat) {
            $cat->pic = Storage::url($cat->pic);
        }
        $products['offer'] = $cats;
        //////////////
        /// دوره های ویژه
        $cats = Category::where("discount", ">", 0)->where("for_sale", "1")->where("status_id", "1")->select("id", "title", "pic", "price", "discount", "for_sale", "score")->get();
        foreach ($cats as $cat) {
            $cat->pic = Storage::url($cat->pic);
        }
        $products['special'] = $cats;
        //////////////
        /// پکیج ها
        $cats = Package::where("status_id", "1")->select("id", "title", "pic", "price", "discount", "score", "for_sale")->get();
        foreach ($cats as $cat) {
            $cat->pic = Storage::url($cat->pic);
        }
        $products['category'] = $cats;
        $return['products'] = $products;
        //////////////////////////////////
        /// اسلایدر ها
        $sliders = Slider::where("status_id", "1")->get();
        foreach ($sliders as $slider) {
            $slider->pic = Storage::url($slider->pic);
        }
        $return['sliders'] = $sliders;
        //صفحات ثابت مثل تماس با ما و ..
        $statics = StaticModel::select("id", "title", "icon")->get();
        foreach ($statics as $static) {
            $static->link = route('showStatic', $static->id);
            if (empty($static->icon))
                $static->icon = '';
            else
                $static->icon = Storage::url($static->icon);

        }
        $return['statics'] = $statics;

        //دریافت نام فایل ها
        $file_names = FileModel::all()->pluck('title','id');
        $return['file_names'] = $file_names;

        return $return;
    }

    public function getCategoryDetails($request)
    {
        //دریافت اطلاعات ریز دوره برای صفحه هر دوره
        $id = $request->input('id');
        //بررسی توکن
        if (empty(config('auth.THIS_USER')))
            throw new \Exception("Not Logged In");
        $user = User::findOrfail(config('auth.THIS_USER'));

        $cat = Category::findOrfail($id);
        //اگر دسته بندی غیرفعال هست به کاربر نشون نده اطلاعات رو
//        if($cat->status_id != '1')
//            throw new \Exception("notEnable");
        //ادرس دهی تصویر دوره
        $cat->pic = Storage::url($cat->pic);
        $cat->description = ($cat->description);
        $cat->lessons = Lesson::where("category_id", $cat->id)->select('title', 'id', 'description')->get();
        $cat->cats = Category::where("parent_id", $cat->id)->select('title', 'id', 'description')->get();

        //اگر کاربر این دوره را خریداری کرده باشه این گزینه ۱ هست
        $cat->owned = (Helper::isPurchased("course", $cat->id, $user->id)) ? '1' : '0';
        $cat->hasExam = 0;
        $cat->stars = Rate::where("type","course")->where("rel_id",$cat->id)->where("user_id",$user->id)->first()->star ?? 0;
        $cat->isFav = Fav::where("user_id",$user->id)->where("type","category")->where("rel_id",$cat->id)->count() > 0 ? '1' : '0';
        if ($cat->owned) {
            //اگر آزمون برای این دوره فعال باشد و کاربر این دوره را خریده باشد این گزینه ۱ هست

            if (empty($cat->exam_detail))
                $cat->hasExam = 0;
            else {
                $exam_detail = json_decode($cat->exam_detail);
                if (($exam_detail->status_id ?? 0) != '1')
                    $cat->hasExam = 0;
                else {
                    $cat->hasExam = 1;
                    $cat->ExamDetail = json_decode($cat->exam_detail);
                    $cat->quizScore = 0;
                    $cat->quizStatus = 0;
                    //check exam history
                    $quiz = Quiz::where("user_id", $user->id)->where("type", "course")->where("rel_id", $id)->where("status_id", "1")->orderBy("created_at", "desc")->get()->first();
                    if (!empty($quiz)) {
                        $cat->quizScore = $quiz->score;
                        $cat->quizStatus = $quiz->status_id;
                    }
                }
            }
        }

        return $cat;
    }

    public function getPackageDetails($request)
    {
        //دریافت اطلاعات پکیج
        $id = $request->input('id');
        if (empty(config('auth.THIS_USER')))
            throw new \Exception("Not Logged In");
        $user = User::findOrfail(config('auth.THIS_USER'));

        $cat = Package::findOrfail($id);
//        if($cat->status_id != '1')
//            throw new \Exception("notEnable");
        $cat->pic = Storage::url($cat->pic);
        $cat->description = ($cat->description);

        //لیست دوره هایی که توی این پکیج هستن
        $categories = json_decode($cat->categories);
        $categories = is_array($categories) ? $categories : [];
        $cat->lessons = Category::whereIn("id", $categories)->get();


        $cat->owned = (Helper::isPurchased("package", $cat->id, $user->id)) ? '1' : '0';
        $cat->hasExam = 0;
        $cat->isFav = Fav::where("user_id",$user->id)->where("type","package")->where("rel_id",$cat->id)->count() > 0 ? '1' : '0';
        $cat->stars = Rate::where("type","package")->where("rel_id",$cat->id)->where("user_id",$user->id)->first()->star ?? 0;
        if ($cat->owned) {
            //اگر آزمون برای این دوره فعال باشد و کاربر این دوره را خریده باشد این گزینه ۱ هست
            if (empty($cat->exam_detail))
                $cat->hasExam = 0;
            else {
                $exam_detail = json_decode($cat->exam_detail);
                if (($exam_detail->status_id ?? 0) != '1')
                    $cat->hasExam = 0;
                else {
                    $cat->hasExam = 1;
                    $cat->ExamDetail = json_decode($cat->exam_detail);
                    $cat->quizScore = 0;
                    $cat->quizStatus = 0;
                    //check exam history
                    $quiz = Quiz::where("user_id", $user->id)->where("type", "course")->where("rel_id", $id)->where("status_id", "1")->orderBy("created_at", "desc")->get()->first();
                    if (!empty($quiz)) {
                        $cat->quizScore = $quiz->score;
                        $cat->quizStatus = $quiz->status_id;
                    }
                }
            }
        }
        return $cat;
    }

    public function getLessonDetails($request)
    {
        //دریافت اطلاعات یک درس
        $id = $request->input('id');
        if(empty(config('auth.THIS_USER')))
            throw new \Exception("Not Logged In");
        $user = User::findOrfail(config('auth.THIS_USER'));

        $cat = Lesson::findOrfail($id);
//        if($cat->status_id != '1')
//            throw new \Exception("notEnable");
        $cat->pic = asset($cat->pic);
        //$cat->description = "'".($cat->description)."'";
        $files = FileModel::where("lesson_id",$cat->id)->select("title","id","type","ext")->get();
        $cat->stars = Rate::where("type","lesson")->where("rel_id",$cat->id)->where("user_id",$user->id)->first()->star ?? 0;

        //لیست فایل ها و نوعشون
        foreach ($files as $file) {
            $type = '-';

            if($file->type == 'video' || $file->type == 'audio'){
                $type = 'فیلم';
                $file->type = 'video';
            }elseif ($file->type == 'pdf'){
                $type = 'پی دی اف';
            }elseif ($file->type == 'other'){
                $type = 'فایل';
            }
            $file->file_type = $type;

        }
        $cat->files = $files;
        $cat->owned = (Helper::isPurchased("lesson",$cat->id,$user->id)) ? '1':'0';
        $cat->isFav = Fav::where("user_id",$user->id)->where("type","lesson")->where("rel_id",$cat->id)->count() > 0 ? '1' : '0';

        $cat->hasExam = 0;
        if($cat->owned) {
            //اگر آزمون برای این دوره فعال باشد و کاربر این دوره را خریده باشد این گزینه ۱ هست
            if(empty($cat->exam_detail))
                $cat->hasExam = 0;
            else{
                $exam_detail = json_decode($cat->exam_detail);
                if(($exam_detail->status_id ?? 0) != '1')
                    $cat->hasExam = 0;
                else {
                    $cat->hasExam = 1;
                    $cat->ExamDetail = json_decode($cat->exam_detail);
                    $cat->quizScore = 0;
                    $cat->quizStatus = 0;
                    //check exam history
                    $quiz = Quiz::where("user_id", $user->id)->where("type", "course")->where("rel_id", $id)->where("status_id", "1")->orderBy("created_at", "desc")->get()->first();
                    if (!empty($quiz)) {
                        $cat->quizScore = $quiz->score;
                        $cat->quizStatus = $quiz->status_id;
                    }
                }
            }
        }
        return $cat;
    }

    public function getStatic($request)
    {
        //دریافت محتویات صفحه ثابت ها
        $id = $request->input('id');
        if (empty(config('auth.THIS_USER')))
            throw new \Exception("Not Logged In");
        $user = User::findOrfail(config('auth.THIS_USER'));
        $static = StaticModel::findOrfail($id);
        return $static->content;
    }

    public function checkDownload($request)
    {
        //کاربر بعد از خرید یک درس وقتی روی یکی از فایل ها کلیک میکنه این تابع فراخوانی میشه
        if (empty(config('auth.THIS_USER')))
            throw new \Exception("Not Logged In");
        $user = User::findOrfail(config('auth.THIS_USER'));
        $file = FileModel::findOrfail($request->input('id'));
        $lesson = Lesson::findOrfail($file->lesson_id);
        //اینجا بررسی میشه که آیا کاربر این دوره رو خریده برای خودش یا نه
        if (!Helper::isPurchased('lesson', $lesson->id, $user->id))
            throw new \Exception("درس مورد نظر خریداری نشده");

        //یک رکورد یکبار مصرف توی جدول دانلود برای این فایل و این کاربر ساخته میشه که بتونه فایل رو دانلود کنه
        $download = new Download();
        $download->file_id = $file->id;
        //ساخت یه توکن موقت برای این فایل
        $download->token = Str::random(150);
        $download->user_id = $user->id;
        $download->save();
        //ارسال لینک موقت دانلود فایل به کاربر برای دانلود کردن محتوا
        return route('dlFile', ['token' => $download->token]);

    }

    /**
     * @param $request
     * @return mixed
     * @throws \Exception
     */
    public function startPay($request)
    {
        //وقتی کاربر اقدام به خرید یک محصول میکنه این تابع فراخوانی میشه
        if (empty(config('auth.THIS_USER')))
            throw new \Exception("Not Logged In");
        $user = User::findOrfail(config('auth.THIS_USER'));
        $id = $request->input('id');
        $type = $request->input('type');
        $different = $request->input('different');
        //بررسی نوع خرید که پکیج هست یا دوره و یا درس
        if ($type == 'lesson') {
            $data = Lesson::findOrfail($id);
            $typ = 'درس';
        } elseif ($type == 'course') {
            $data = Category::findOrfail($id);
            $typ = 'دوره';
        } elseif ($type == 'package') {
            $data = Package::findOrfail($id);
            $typ = 'پکیج';
        } else throw new \Exception("نوع خرید مشخص نیست");

        //بررسی فعال خرید بودن محصول
        if ($data->status_id != '1')
            throw new \Exception("محصول قابل خرید نیست");
        //بررسی قابل خرید بودن محصول
        if ($data->for_sale != '1')
            throw new \Exception("محصول قابل خرید نیست");
        //بررسی اینکه آیا کاربر قبلا این محصول رو خریده یا نه(چنین چیزی پیش نمیاد ولی برای اطمینان گذاشتم)
        $count = UserLesson::where("type", $type)->where("user_id", $user->id)->where("rel_id", $id)->count();
        if ($count > 0)
            throw new \Exception("شما این دوره را قبلا خریداری کرده اید !");
        $price = $data->price - $data->discount - $different;
        $discount = 0;
        $coupon_id = 0;
        if($request->input('code') !== null && $request->input('code') !== '' ) {
            try {
                //اگر کد تخفیف وارد شده باشه این تابع کد رو دریافت میکنه و پس از بررسی صحت قیمت ها رو تغییر میده
                $coupon = $this->checkCoupon($request);
                $price = $coupon['total'] ?? $price;
                $discount = $coupon['discount'] ?? 0;
                $coupon_id = $coupon['id'] ?? 0;

            } catch (\Exception $e) {
                //
            }
        }
        $fullyPayed = false;
        if ($request->input('wallet') == '1') {
            //اگر درخواست از این طریق باشه که کاربر بخاد بخشی از مبلغ رو از کیف پول پرداخت کنه این عملیات انجام یشه

            //بررسی اینکه آیا کاربر اعتبار داره یانه
            if ($user->credit > 0) {
                //اگر اعتبار بیشتر یا مساوی  مبلغ خرید بود
                if ($user->credit >= $price) {
                    $fullyPayed = true;
                    //مبلغ خرید از موجودی کاربر کسر میشه
                    $user->credit -= $price;
                    $user->save();
                    //trans
                    $transaction = new Transaction();
                    $transaction->user_id = $user->id;
                    $transaction->type = $type;
                    $transaction->rel_id = $data->id;
                    $transaction->amount = $price;
                    $transaction->discount = $discount;
                    $transaction->coupon_id = $coupon_id;
                    $transaction->status_id = '1';
                    $transaction->res_id = 'پرداخت از اعتبار کاربری برای خرید ' . $typ . ' ' . $data->title;
                    $transaction->ref_id = '';
                    $transaction->save();
                } else {
                    //اگر اعتبار از مبلغ خرید کمتر بود
                    $price -= $user->credit;
                    //مبلغ اعتبار از مبلغ خرید کسر میشه و موجودی صفر میشه
                    $user->credit = 0;
                    $user->save();
                    $transaction = new Transaction();
                    $transaction->user_id = $user->id;
                    $transaction->type = $type;
                    $transaction->rel_id = $data->id;
                    $transaction->amount = $user->credit;
                    $transaction->discount = 0;
                    $transaction->coupon_id = $coupon_id;
                    $transaction->status_id = '1';
                    $transaction->res_id = 'پرداخت از اعتبار کاربری برای بخشی از خرید ' . $typ . ' ' . $data->title;
                    $transaction->ref_id = '';
                    $transaction->save();
                }
            }
        }
        if ($price <= 0) {
            $fullyPayed = true;
        }

        if ($fullyPayed) {
            //اگر کل مبلغ با اعتبار پرداخت بشه محصول مورد نظر برای کاربر فعال میشه و دیگه نیازی به درگاه پرداخت نیست
            $userLesson = new UserLesson();
            $userLesson->user_id = $user->id;
            $userLesson->type = $type;
            $userLesson->rel_id = $data->id;
            $userLesson->transaction_id = $data->id;
            $userLesson->save();
            $type = $type;

            if (!empty($data)) {
                $prize = $data->score;
                $user->credit += $prize;
                $user->save();
            }
            return route('success');


        } else {
            //اگر مقداری از مبلغ کالا مونده باشه یک تراکنش برای کاربر ثبت میشه و کاربر برای پرداخت مبلغ به درگاه پرداخت هدایت میشه
            $transaction = new Transaction();
            $transaction->user_id = $user->id;
            $transaction->type = $type;
            $transaction->rel_id = $data->id;
            $transaction->amount = $price;
            $transaction->discount = $discount;
            $transaction->coupon_id = $coupon_id;
            $transaction->status_id = '0';
            $transaction->res_id = '';
            $transaction->ref_id = '';
            $transaction->save();
            return route('payment', ['id' => $transaction->id, 'token' => $request->input('token')]);
        }
    }

    public function getUserProfile()
    {
        //دریافت اطلاعات پروفایل کاربری
        if (empty(config('auth.THIS_USER')))
            throw new \Exception("Not Logged In");
        $user = User::findOrfail(config('auth.THIS_USER'));
        return [
            "name" => $user->name,
            "state" => $user->state,
            "parent_name" => $user->parent_name,
            "b_y" => $user->b_y,
            "b_m" => $user->b_m,
            "b_d" => $user->b_d,
            "melicode" => $user->melicode,
            "r_s" => $user->r_s,
            "r_m" => $user->r_m,
            "address" => $user->address,
            "zipcode" => $user->zipcode,
            "city" => $user->city,
            "phone" => $user->phone,
            "email" => $user->email,
            "id" => $user->id,
            "country" => $user->country,
            "pic" => Storage::url($user->pic ?? 'man.png')
        ];

    }

    public function ChangeAvatar($request)
    {
        //تغییر آواتار کاربر
        if (empty(config('auth.THIS_USER')))
            throw new \Exception("Not Logged In");
        $user = User::findOrfail(config('auth.THIS_USER'));
        $image = $request->file('avatar');
        //ساخت یک اسم یکتا برای تصویر پروفایل

        //آپلود در پوشه مورد نظر
        $file_name = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME);
        $saved_name = $file_name.substr(0,10).'-'.md5(time());
        $path = Storage::putFile('uploads/User'.$saved_name . '.' . $image->getClientOriginalExtension(), $image);

        //ذخیره آدرس در جدول کاربران
        $user->pic = $path;
        $user->save();
        return "1";


    }

    public function saveUserProfile($request)
    {
        //ذخیره اطلاعات پروفایل کاربری
        if (empty(config('auth.THIS_USER')))
            throw new \Exception("Not Logged In");
        $fields = ['name','parent_name','b_y','b_m','b_d','melicode','r_s','r_m','country','state','city','address','zipcode','email','phone'];
        $user = User::findOrfail(config('auth.THIS_USER'));
        foreach ($fields as $field){
            $user->$field = $request->input($field);
        }
        $user->save();
        return "1";
    }

    public function ChangePersonalPic($request)
    {
        //تغییر آواتار کاربر
        if(empty(config('auth.THIS_USER')))
            throw new \Exception("Not Logged In");
        $user = User::findOrfail(config('auth.THIS_USER'));
        $image = $request->file('avatar');
        //ساخت یک اسم یکتا برای تصویر پروفایل
        $file_name = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME);
        $saved_name = $file_name.substr(0,10).'-'.md5(time());
        $path = Storage::putFile('uploads/ProfilePic/'.$saved_name . '.' . $image->getClientOriginalExtension(), $image);
        $user->personal_pic = $path;
        $user->save();
        return "1";
    }

    public function ChargeWallet($request)
    {
        //شارژ کیف پول کاربر
        if (empty(config('auth.THIS_USER')))
            throw new \Exception("Not Logged In");
        $user = User::findOrfail(config('auth.THIS_USER'));
        $amount = $request->input('amount');
        //بررسی صحت مبلغ وارد شده
        if ($amount == 0)
            throw new \Exception("لطفا مبلغ صحیح وارد کنید");
        $transaction = new Transaction();
        $transaction->user_id = $user->id;
        $transaction->type = 'wallet';
        $transaction->rel_id = 0;
        $transaction->amount = $amount;
        $transaction->status_id = '0';
        $transaction->res_id = '';
        $transaction->ref_id = '';
        $transaction->save();
        //ارسال به درگاه پرداخت
        return route('payment', ['id' => $transaction->id, 'token' => $request->input('token')]);
    }

    public function getUserCourses($request)
    {
        //دریافت لیست کالاهای خریداری شده توسط کاربر
        if (empty(config('auth.THIS_USER')))
            throw new \Exception("Not Logged In");
        $user = User::findOrfail(config('auth.THIS_USER'));
        $ls = UserLesson::where("user_id", $user->id)->orderBy("created_at", "desc")->get();
        foreach ($ls as $l) {
            //قرار دادن نام هر محصول در لیست
            $l->titleName = $l->title;
        }
        return $ls;
    }

    public function getCourses($request)
    {
        //دریافت لیست همه دوره ها
        $cats = Category::where("status_id", "1")->select("id", "title", "pic", "price", "discount", "for_sale", "score");

        //اگر کاربر کاراکتر خاصی رو جستجو کرده باشه
        if (!empty($request->input('q')))
            $cats = $cats->where("title", "LIKE", "%" . $request->input('q') . "%");

        // اگر کاربر مرتب سازی خاصی رو مد نظر داشته
        if (!empty($request->input('sort_type'))) {
            $sort = $request->input('sort_type');
            if ($sort == 'date')
                $cats = $cats->orderBy("created_at", "desc");
            if ($sort == 'pricelowest')
                $cats = $cats->orderBy("price", "asc");
            if ($sort == 'pricehighest')
                $cats = $cats->orderBy("price", "desc");

        }

        $cats = $cats->get();

        foreach ($cats as $cat) {
            $cat->pic = Storage::url($cat->pic);
        }
        return $cats;
    }

    public function userTransactionHistory($request)
    {
        //لیست تراکنش های کاربر
        if (empty(config('auth.THIS_USER')))
            throw new \Exception("Not Logged In");
        $user = User::findOrfail(config('auth.THIS_USER'));
        $transactions = Transaction::where("user_id", $user->id)->where("status_id", "1")->select("id", "amount", "created_at", "res_id", "rel_id", "type")->orderBy("created_at", "desc")->get();
        foreach ($transactions as $transaction) {
            //اضافه کردن تاریخ تراکنش به لیست
            $transaction->created = jdate($transaction->created_at)->format("l d F Y");
            //اگر تراکنش برای شارژ کیف پول بوده
            if ($transaction->rel_id == 0)
                $transaction->title = "افزایش اعتبار حساب کاربری";
            else {
                //در غیر این صورت مشخص کردن اینکه برای خرید چه چیزی بوده
                $type = $transaction->type;
                $id = $transaction->rel_id;
                if ($type == 'lesson') {
                    $data = Lesson::findOrfail($id);
                    $type = 'درس';
                } elseif ($type == 'course') {
                    $data = Category::findOrfail($id);
                    $type = 'دوره';
                } elseif ($type == 'package') {
                    $data = Package::findOrfail($id);
                    $type = 'پکیج';
                } elseif ($type == 'quiz') {
                    $type = 'آزمون';
                }

                $transaction->title = "خرید " . $type . " " . ($data->title ?? '');

            }
        }
        return $transactions;
    }

    public function checkCoupon($request)
    {
        //بررسی کد تخفیف
        if (empty(config('auth.THIS_USER')))
            throw new \Exception("Not Logged In");
        $user = User::findOrfail(config('auth.THIS_USER'));
        $code = $request->input('code');
        $id = $request->input('id');
        $type = $request->input('type');
        if ($type == 'course') {
            $data = Category::findOrfail($id);
        } elseif ($type == 'lesson') {
            $data = Lesson::findOrfail($id);
        } elseif ($type == 'package') {
            $data = Package::findOrfail($id);
        }

        $coupon = Coupon::where("code", $code);
        //اگر کد تخفیف وجود دارد در جدول
        if ($coupon->count() == 0)
            throw new \Exception("کد تخفیف صحیح نیست");
        $coupon = $coupon->get()->first();
        //اگر این کد برای این کاربر قابل استفاده هست
        if (!$coupon->canUse($user->id))
            throw new \Exception("این کد تخفیف باطل شده است");

        $price = $data->price - $data->discount;
        $percent = $coupon->percent;
        $discount = round($price * ($percent / 100));
        if ($discount > $coupon->max)
            $discount = $coupon->max;
        $total = round($price - $discount);

        $discount = $discount + $data->discount;
        //برگرداندن درصد و مبلغ
        return ["percent" => $percent, "discount" => $discount, "total" => $total, "id" => $coupon->id];
    }

    public function getQuizDetails($request)
    {
        if (empty(config('auth.THIS_USER')))
            throw new \Exception("Not Logged In");
        $user = User::findOrfail(config('auth.THIS_USER'));

        $type = $request->input('type');
        $id = $request->input('id');
        if ($type == 'lesson') {
            $data = Lesson::findOrfail($id);
            $type = 'درس';
        } elseif ($type == 'course') {
            $data = Category::findOrfail($id);
            $type = 'دوره';
        } elseif ($type == 'package') {
            $data = Package::findOrfail($id);
            $type = 'پکیج';
        } else throw new \Exception("نوع محصول قابل شناسایی نیست");
        if (empty($data->exam_detail))
            throw new \Exception("آزمونی برای این محصول اراپه نشده");

        if (!Helper::isPurchased($request->input('type'), $data->id, $user->id))
            throw new \Exception("این محصول خریداری نشده !");

        $detail = json_decode($data->exam_detail);
        $detail->payable = 0;
        $detail->product_name = $type . ' ' . $data->title;
        $detail->total_question = ((int)$detail->hard ?? 0) + ((int)$detail->normal ?? 0) + ((int)$detail->easy ?? 0);

        $quiz = Quiz::where("user_id", $user->id)->where("type", $request->input('type'))->where("rel_id", $data->id);
        if ($quiz->count() >= $detail->max_use) {
            $price = number_format($detail->extra_price);
            $detail->extra = "شما حداکثر تعداد شرکت در این آزمون را انجام داده اید . برای آزمون مجدد مبلغ " . $price . " تومان از اعتبار حساب کاربری شما کسر خواهد شد.";
            $detail->payable = $detail->extra_price;
        }
        return $detail;
    }

    public function startQuiz($request)
    {
        if (empty(config('auth.THIS_USER')))
            throw new \Exception("Not Logged In");
        $user = User::findOrfail(config('auth.THIS_USER'));

        $type = $request->input('type');
        $id = $request->input('id');
        if ($type == 'lesson') {
            $data = Lesson::findOrfail($id);
            $type = 'درس';
        } elseif ($type == 'course') {
            $data = Category::findOrfail($id);
            $type = 'دوره';
        } elseif ($type == 'package') {
            $data = Package::findOrfail($id);
            $type = 'پکیج';
        } else throw new \Exception("نوع محصول قابل شناسایی نیست");

        if (empty($data->exam_detail))
            throw new \Exception("آزمونی برای این محصول اراپه نشده");

        if (!Helper::isPurchased($request->input('type'), $data->id, $user->id))
            throw new \Exception("این محصول خریداری نشده !");


        //تحلیل تنظیمات آزمون
        $detail = json_decode($data->exam_detail);
        $detail->product_name = $type . ' ' . $data->title;
        $detail->total_question = ((int)$detail->hard ?? 0) + ((int)$detail->normal ?? 0) + ((int)$detail->easy ?? 0);
        $AnsCount = (empty($detail->answer_count) ? 4 : $detail->answer_count);
        //بررسی سوابق آزمون کاربر
        $quiz = Quiz::where("user_id", $user->id)->where("type", $request->input('type'))->where("rel_id", $data->id);
        //کسر هزینه در صورت استفاده بیش از حد از ازمون
        if ($quiz->count() >= $detail->max_use) {
            //بررسی موجودی حساب کاربر
            if ($user->credit < $detail->extra_price)
                throw new \Exception("موجودی حساب کاربری شما کافی نیست . لطفا ابتدا حساب کاربری خود را به مبلغ " . number_format($detail->extra_price) . " تومان شارژ کنید");

            $trans = new Transaction();
            $trans->user_id = $user->id;
            $trans->type = "quiz";
            $trans->rel_id = $data->id;
            $trans->res_id = "خرید مجدد آزمون " . $detail->product_name;
            $trans->ref_id = "";
            $trans->status_id = '1';
            $trans->amount = $detail->extra_price;
            $trans->save();
            $user->credit -= $detail->extra_price;
            $user->save();
        }


        $lessons = [];
        $theType = $request->input('type');
        if ($theType == 'lesson') {
            $lessons[] = $data->id;
        } else {
            $lessons = Helper::getAllLessons($data->id, $theType);
        }
        //بررسی موجود بودن تعداد سوالات
        if (($detail->easy + $detail->normal + $detail->hard) == 0)
            throw new \Exception("در تحلیل بانک اطلاعات خطایی رخ داد . لطفا با پشتیبانی تماس بگیرید.");
        //دریافت سوالات از پایگاه داده
        $easy = Question::whereIn("lesson_id", $lessons)->where("level", "1")->select('id', 'title', 'level', 'lesson_id');
        $normal = Question::whereIn("lesson_id", $lessons)->where("level", "2")->select('id', 'title', 'level', 'lesson_id');
        $hard = Question::whereIn("lesson_id", $lessons)->where("level", "3")->select('id', 'title', 'level', 'lesson_id');

        //بررسی موجود بودن تعداد سوالات
        if ($easy->count() < $detail->easy || $normal->count() < $detail->normal || $hard->count() < $detail->hard)
            throw new \Exception("در تحلیل بانک اطلاعات خطایی رخ داد . لطفا با پشتیبانی تماس بگیرید.");
        $easy = $easy->inRandomOrder()->take($detail->easy);
        $normal = $normal->inRandomOrder()->take($detail->normal);
        $hard = $hard->inRandomOrder()->take($detail->hard);

        $easy = $easy->get();
        $normal = $normal->get();
        $hard = $hard->get();

        //ترکیب و میکس سوالات
        $questions = $easy->concat($normal)->concat($hard)->shuffle();


        //ساخت آزمون جدید
        $userQuiz = new Quiz();
        $userQuiz->user_id = $user->id;
        $userQuiz->type = $request->input('type');
        $userQuiz->rel_id = $data->id;
        $userQuiz->status_id = 0;
        $userQuiz->save();
        $detail->quiz_id = $userQuiz->id;

        //افزودن سوالات به دیتابیس
        foreach ($questions as $question) {
            $qq = new QuizQuestion();
            $qq->quiz_id = $userQuiz->id;
            $qq->user_id = $user->id;
            $qq->question_id = $question->id;//
            $qq->save();

            //جواب صحیح سوال
            $trueAnswer = QuestionAnswer::where("question_id", $question->id)->where("is_true", "1")->select("id", "question_id", "value")->inRandomOrder()->take(1)->get();

            $answers = QuestionAnswer::where("question_id", $question->id)->where("is_true", "2")->select("id", "question_id", "value")->inRandomOrder()->take($AnsCount - 1);
            $answers = $answers->get();
            $question->id = $qq->id;
            $question->answers = [];

            if (!empty($trueAnswer) && !empty($answers)) {
                $answers = $answers->concat($trueAnswer)->shuffle();

                $question->answers = $answers;
                foreach ($answers as $answer) {
                    $qqa = new QuizQuestionAnswer();
                    $qqa->question_id = $qq->id;
                    $qqa->answer_id = $answer->id;
                    $qqa->user_select = 0;
                    $qqa->save();
                }
            }
        }
        return ["details" => $detail, "questions" => $questions];
    }

    public function setQuizAnswer($request)
    {
        if (empty(config('auth.THIS_USER')))
            throw new \Exception("Not Logged In");
        $user = User::findOrfail(config('auth.THIS_USER'));

        //get last quiz
        $quiz = Quiz::where("user_id", $user->id)->where('id', $request->input('quiz_id'));
        if ($quiz->count() == 0)
            throw new \Exception("TIME_EXCEEDED");
        $quiz = $quiz->get()->first();
        $id = $quiz->rel_id;
        $type = $quiz->type;
        if ($type == 'lesson') {
            $data = Lesson::findOrfail($id);
            $type = 'درس';
        } elseif ($type == 'course') {
            $data = Category::findOrfail($id);
            $type = 'دوره';
        } elseif ($type == 'package') {
            $data = Package::findOrfail($id);
            $type = 'پکیج';
        } else throw new \Exception("نوع آزمون قابل شناسایی نیست");
        $detail = json_decode($data->exam_detail);
        if (empty($detail))
            throw new \Exception("تنظیمات آزمون شناسایی نشد");
        if ($quiz->created_at->diffInMinutes(Carbon::now()) > $detail->exam_time)
            throw new \Exception("TIME_EXCEEDED");
        $answer = QuizQuestionAnswer::where("question_id", $request->input('question_id'))->update(["user_select" => "0"]);
        $answer = QuizQuestionAnswer::where("question_id", $request->input('question_id'))->where("answer_id", $request->input('value'))->get()->first();
        if (!empty($answer)) {
            $answer->user_select = '1';
            $answer->save();
        }
        return "1";
    }

    public function getExamDetails($request)
    {
        if (empty(config('auth.THIS_USER')))
            throw new \Exception("Not Logged In");
        $user = User::findOrfail(config('auth.THIS_USER'));

        //get last quiz
        $quiz = Quiz::where("user_id", $user->id)->where('id', $request->input('quiz_id'));
        if ($quiz->count() == 0)
            throw new \Exception("TIME_EXCEEDED");
        $quiz = $quiz->get()->first();
        $id = $quiz->rel_id;
        $type = $quiz->type;
        if ($type == 'lesson') {
            $data = Lesson::findOrfail($id);
            $type = 'درس';
        } elseif ($type == 'course') {
            $data = Category::findOrfail($id);
            $type = 'دوره';
        } elseif ($type == 'package') {
            $data = Package::findOrfail($id);
            $type = 'پکیج';
        } else throw new \Exception("نوع آزمون قابل شناسایی نیست");
        $detail = json_decode($data->exam_detail);
        if (empty($detail))
            throw new \Exception("تنظیمات آزمون شناسایی نشد");
        if ($quiz->created_at->diffInMinutes(Carbon::now()) > $detail->exam_time)
            throw new \Exception("TIME_EXCEEDED");
        $time = $detail->exam_time - $quiz->created_at->diffInMinutes(Carbon::now());
        $questions = QuizQuestion::where("quiz_id", $quiz->id)->get()->pluck("id")->toArray();
        if ($time < 1)
            throw new \Exception("TIME_EXCEEDED");
        return ['remainTime' => $time, 'selected' => QuizQuestionAnswer::whereIn("question_id", $questions)->get()->pluck("user_select", "question_id")->toArray()];
    }

    public function getUserExams($request)
    {
        if (empty(config('auth.THIS_USER')))
            throw new \Exception("Not Logged In");
        $user = User::findOrfail(config('auth.THIS_USER'));
        $quizs = Quiz::where("user_id", $user->id)->orderBy("created_at", "desc")->get();
        foreach ($quizs as $quiz) {
            $quiz->status = ($quiz->status_id == '1' ? ' قبول' : ($quiz->status_id == '2' ? 'مردود' : '-مردود'));
            try {
                $type = $quiz->type;
                $id = $quiz->rel_id;

                if ($type == 'lesson') {
                    $data = Lesson::findOrfail($id);
                    $type = 'درس';
                } elseif ($type == 'course') {
                    $data = Category::findOrfail($id);
                    $type = 'دوره';
                } elseif ($type == 'package') {
                    $data = Package::findOrfail($id);
                    $type = 'پکیج';
                } else throw new \Exception("نوع آزمون قابل شناسایی نیست");
                $quiz->titleName = $data->title;
                $quiz->created = jdate($quiz->created_at)->format("l d F Y");
            } catch (\Exception $e) {

            }
        }
        return $quizs;


    }

    public function getExamResult($request)
    {
        if (empty(config('auth.THIS_USER')))
            throw new \Exception("Not Logged In");
        $user = User::findOrfail(config('auth.THIS_USER'));

        //get last quiz
        $quiz = Quiz::where("user_id", $user->id)->where('id', $request->input('id'));
        if ($quiz->count() == 0)
            throw new \Exception("آزمون معتبر نیست");
        $quiz = $quiz->get()->first();
        $id = $quiz->rel_id;
        $type = $quiz->type;
        if ($type == 'lesson') {
            $data = Lesson::findOrfail($id);
            $type = 'درس';
        } elseif ($type == 'course') {
            $data = Category::findOrfail($id);
            $type = 'دوره';
        } elseif ($type == 'package') {
            $data = Package::findOrfail($id);
            $type = 'پکیج';
        } else throw new \Exception("نوع آزمون قابل شناسایی نیست");
        $detail = json_decode($data->exam_detail);


        $questions = QuizQuestion::where("quiz_id", $quiz->id)->get()->pluck('id')->toArray();

        $true = 0;
        $answers = QuizQuestionAnswer::whereIn("question_id", $questions)->get();
        foreach ($answers as $answer) {
            if (($answer->answer->is_true ?? '0') == '1' && $answer->user_select == '1') {
                $true++;
            }
        }
        $total = count($questions);
        if (empty($total))
            $total = 1;
        $max_score = $detail->max_score;
        $min_score = $detail->min_score;
        $score = round(($max_score * $true) / $total);
        if ($score >= $min_score)
            $quiz->status_id = '1';
        else
            $quiz->status_id = '2';

        $quiz->score = $score;
        $quiz->save();
        $questions = QuizQuestion::where("quiz_id", $quiz->id)->get();
        foreach ($questions as $question) {
            $questionB = Question::where("id", $question->question_id)->select('id', 'title', 'level')->get()->first();
            $question->title = $questionB->title ?? '';
            $question->level = $questionB->level ?? '';
            $answers = QuizQuestionAnswer::where("question_id", $question->id)->get();

            foreach ($answers as $answer) {
                $ans = QuestionAnswer::find($answer->answer_id);
                $answer->title = $ans->value ?? '-';
                $answer->is_true = $ans->is_true ?? '-';
            }
            $question->answers = $answers;
        }
        return ["true" => $true, "false" => $total - $true, "total" => $total, "score" => $score, "min_score" => (int)$detail->min_score, "status" => $quiz->StatusText];

    }

    public function getUserFavCourses($request)
    {
        //دریافت لیست کالاهای لایک شده توسط کاربر
        if(empty(config('auth.THIS_USER')))
            throw new \Exception("Not Logged In");
        $user = User::findOrfail(config('auth.THIS_USER'));
        $ls = Fav::where("user_id",$user->id)->orderBy("created_at","desc")->get();

        foreach ($ls as $l) {
            if(empty($l->rel)) {
                $l->delete();
                return $this->getUserFavCourses($request);
            }
            //قرار دادن نام هر محصول در لیست
            $l->titleName = $l->rel->title ?? '-';
        }
        return $ls;
    }

    public function addFav($request)
    {
        if(empty(config('auth.THIS_USER')))
            throw new \Exception("Not Logged In");
        $user = User::findOrfail(config('auth.THIS_USER'));

        if(is_array($request->input('id'))) {
            foreach ($request->input('id') as $id) {
                $chk = Fav::where("user_id", $user->id)->where("id", $id)->first();
                if(!empty($chk))
                    $chk->delete();
            }
            return "1";
        }

        $chk = Fav::where("user_id",$user->id)->where("type",$request->input('type'))->where("rel_id",$request->input('id'))->first();
        if(empty($chk)){
            $chk = new Fav();
            $chk->user_id = $user->id;
            $chk->type = $request->input('type');
            $chk->rel_id = $request->input('id');
            $chk->save();
        }else{
            $chk->delete();
        }
        return "1";
    }
    public function setStar($request)
    {
        if(empty(config('auth.THIS_USER')))
            throw new \Exception("Not Logged In");

        $id = $request->input('id');
        $type = $request->input('type');
        if($type == 'lesson'){
            $data = Lesson::findOrfail($id);
            $type = 'درس';
        }elseif($type == 'course'){
            $data = Category::findOrfail($id);
            $type = 'دوره';
        }elseif($type == 'package'){
            $data = Package::findOrfail($id);
            $type = 'پکیج';
        }else throw new \Exception("نوع آزمون قابل شناسایی نیست");

        $user = User::findOrfail(config('auth.THIS_USER'));
        $rate = Rate::where("type",$request->input('type'))->where("rel_id",$request->input('id'))->where("user_id",$user->id)->get()->first();
        if(empty($rate))
            $rate = new Rate();

        $rate->type = $request->input('type');
        $rate->rel_id = $request->input('id');
        $rate->user_id = $user->id;
        $rate->star = $request->input('star');
        $rate->save();

        $data->stars = round(Rate::where("type",$request->input('type'))->where("rel_id",$request->input('id'))->avg('star'));
        $data->save();
        return "1";
    }

    public function getDifferentPrice($request)
    {
        if(empty(config('auth.THIS_USER')))
            throw new \Exception("Not Logged In");
        $differentPrice = 0;
        $user = User::findOrFail(config('auth.THIS_USER'));
        $id = $request->input('id');
        $type = $request->input('type');
        if($type == 'lesson'){
            return 0;
        }elseif($type == 'course'){
            $course = Category::findOrFail($id);
            $courseLessons = $course->lessons->pluck('id');
            $purchasedLessonsId = UserLesson::where('type','lesson')
                ->where('user_id',$user->id)
                ->whereIn('rel_id',$courseLessons)->pluck('rel_id');
            $purchasedLessons = Lesson::whereIn('id',$purchasedLessonsId)->get();
            foreach ($purchasedLessons as $lesson){
                $differentPrice += $lesson->price - $lesson->discount;
            }
            return $differentPrice;

        }elseif($type == 'package'){
            $package = Package::findOrFail($id);
            $packageCourses = $package->courses->pluck('id');
            $purchasedCoursesId = UserLesson::where('type','course')
                ->where('user_id',$user->id)
                ->whereIn('rel_id',$packageCourses)->pluck('rel_id');
            $purchasedCourses = Lesson::whereIn('id',$purchasedCoursesId)->get();
            foreach ($purchasedCourses as $course){
                $differentPrice += $course->price - $course->discount;
            }
            $packageLessons = $package->lessons->pluck('id');
            $purchasedLessonsId = UserLesson::where('type','lesson')
                ->where('user_id',$user->id)
                ->whereIn('rel_id',$packageLessons)->pluck('rel_id');
            $purchasedLessons = Lesson::whereIn('id',$purchasedLessonsId)->get();
            foreach ($purchasedLessons as $lesson){
                $differentPrice += $lesson->price - $lesson->discount;
            }
            return $differentPrice;
        }else throw new \Exception("نوع محتوا قابل شناسایی نیست");
    }
}
