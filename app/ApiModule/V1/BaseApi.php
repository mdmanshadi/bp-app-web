<?php
/**
 *  Copyright (c) 2019.
 *  Developer : Mehdi Meshkatian :)
 */

namespace App\ApiModule\V1;

use App\ApiModule\Module\BaseApiModule;
use App\ApiModule\MainApi;

class BaseApi extends MainApi {
    use BaseApiModule;
}
